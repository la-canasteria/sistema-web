﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADError
    {
        public int registrarLog(CENRequestErrorWeb datos) {
            int respuesta = 0;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_registrar_log_web", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_sistema", MySqlDbType.VarChar).Value = datos.sistema;
                cmd.Parameters.Add("p_modulo", MySqlDbType.VarChar).Value = datos.modulo;
                cmd.Parameters.Add("p_opcion", MySqlDbType.VarChar).Value = datos.opcion;
                cmd.Parameters.Add("p_tipo", MySqlDbType.VarChar).Value = datos.tipo;
                cmd.Parameters.Add("p_descripcion", MySqlDbType.LongText).Value = datos.descripcion;
                cmd.Parameters.Add("p_usuario", MySqlDbType.VarChar).Value = datos.usuario;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    respuesta = Convert.ToInt32(dr["transaccion"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return respuesta;
        }
    }
}
