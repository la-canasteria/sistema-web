﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADVentasBancos
    {
        private MySqlConnection conexion;
        public List<CENVentasBancos> CADReporteVentasBancos(string fechin, string fechfin, string banco)
        {

            MySqlDataReader dr = null;
            CADConexionMySQL conex = new CADConexionMySQL();
            List<CENVentasBancos> listareporte = new List<CENVentasBancos>();

            conexion = new MySqlConnection(conex.CxMySQL());
            conexion.Open();
            try
            {
                //baja Categoria
                using (MySqlCommand command2 = new MySqlCommand("pa_dbcanasteria_vt_mostrar_ventasxbancos", conexion))
                {
                    string fechai = "";
                    string fechaf = "";

                    if (String.IsNullOrEmpty(fechin))
                    {
                        fechai = "";
                    }
                    else
                    {
                        fechai = Convert.ToDateTime(fechin).ToString("yyyy/MM/dd");
                    }

                    if (String.IsNullOrEmpty(fechfin))
                    {
                        fechaf = "";
                    }
                    else
                    {
                        fechaf = Convert.ToDateTime(fechfin).ToString("yyyy/MM/dd");
                    }

                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.Add("@p_fechain", MySqlDbType.String).Value = fechai;
                    command2.Parameters.Add("@p_fechafi", MySqlDbType.String).Value = fechaf;
                    command2.Parameters.Add("@p_banco", MySqlDbType.String).Value = banco;
                    command2.CommandTimeout = 0;
                    dr = command2.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CENVentasBancos cenreporte = new CENVentasBancos();
                            cenreporte.descbanco = dr["descbanco"].ToString();
                            cenreporte.tarjeta = dr["tarjeta"].ToString();
                            cenreporte.impdscto = Convert.ToDecimal(dr["impdscto"].ToString());
                            cenreporte.cantive = Convert.ToInt32(dr["cantive"].ToString());
                            cenreporte.imptven = Convert.ToDecimal(dr["impvent"].ToString());
                            listareporte.Add(cenreporte);
                        }
                    }
                }
                return listareporte;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

        }
    }
}
