﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace CAD
{
    public class CADDescuento
    {
        public CENMensajeDescuento reg_edit_descuento(CENRegistrarDescuento datos)
        {
            CENMensajeDescuento mensajeRegistrar = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            TimeSpan horaI = new TimeSpan();
            TimeSpan horaF = new TimeSpan();

            try
            {
                horaI = TimeSpan.Parse(datos.horaI);
                horaF = TimeSpan.Parse(datos.horaF);

                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_descuento_reg_edit", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("proceso", MySqlDbType.Int32).Value = datos.proceso;
                cmd.Parameters.Add("fechaI", MySqlDbType.Date).Value = ConvertFechaStringToDate(datos.fechaVigenciaI);
                cmd.Parameters.Add("fechaF", MySqlDbType.Date).Value = ConvertFechaStringToDate(datos.fechaVigenciaF);
                cmd.Parameters.Add("horaI", MySqlDbType.Time).Value = horaI;
                cmd.Parameters.Add("horaF", MySqlDbType.Time).Value = horaF;
                cmd.Parameters.Add("codestado", MySqlDbType.Int32).Value = datos.flagestado;
                cmd.Parameters.Add("marca", MySqlDbType.Int32).Value = datos.codMarca;
                cmd.Parameters.Add("tipo", MySqlDbType.Int32).Value = datos.CodTipo;
                cmd.Parameters.Add("banco", MySqlDbType.VarChar).Value = datos.CodBanco;
                cmd.Parameters.Add("categoria", MySqlDbType.Int32).Value = datos.CodCategoria;
                cmd.Parameters.Add("cantidad", MySqlDbType.Decimal).Value = datos.codCantidad;
                cmd.Parameters.Add("descrip", MySqlDbType.VarChar, 250).Value = datos.descripcion;
                cmd.Parameters.Add("ntradesc", MySqlDbType.Int32).Value = datos.ntraDescuento;
                cmd.Parameters.Add("nomUsuario", MySqlDbType.VarChar).Value = datos.nomUsuario;

                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    mensajeRegistrar = new CENMensajeDescuento();
                    mensajeRegistrar.flag = Convert.ToInt32(dr["flag"]);
                    mensajeRegistrar.mensaje = Convert.ToString(dr["mensaje"]);
                    mensajeRegistrar.ntraDescuento = Convert.ToInt32(dr["codigo"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mensajeRegistrar;
        }

        public List<CENListarDescuento> ListarDescuento(int codMarca, int CodTipo, string codBanco, int categoria, int codEstado, string codFechaI, string codFechaF)
        {
            List<CENListarDescuento> listaDeatlle = new List<CENListarDescuento>();
            
            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_descuento_listar", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("codMarca", MySqlDbType.Int32).Value = codMarca;
                cmd.Parameters.Add("codTipo", MySqlDbType.Int32).Value = CodTipo;
                cmd.Parameters.Add("codBanco", MySqlDbType.VarChar).Value = codBanco;
                cmd.Parameters.Add("codCategoria", MySqlDbType.Int32).Value = categoria;
                cmd.Parameters.Add("codEstado", MySqlDbType.Int32).Value = codEstado;
                if (codFechaI == "" )
                {
                    cmd.Parameters.Add("fechaI", MySqlDbType.Date).Value = null;
                }
                else
                {
                    cmd.Parameters.Add("fechaI", MySqlDbType.Date).Value = ConvertFechaStringToDate(codFechaI);
                }

                if (codFechaF == "" )
                {
                    cmd.Parameters.Add("fechaF", MySqlDbType.Date).Value = null;
                }
                else
                {
                    cmd.Parameters.Add("fechaF", MySqlDbType.Date).Value = ConvertFechaStringToDate(codFechaF);
                }

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CENListarDescuento objlistadescuento = new CENListarDescuento();
                    objlistadescuento.ntraDescuento = Convert.ToInt32(dr["ntraDescuento"]);
                    objlistadescuento.descripcion = Convert.ToString(dr["descripcion"]);
                    objlistadescuento.fechaInicial = Convert.ToDateTime(dr["fechaInicial"]).ToString("dd/MM/yyyy");
                    objlistadescuento.fechaFin = Convert.ToDateTime(dr["fechaFin"]).ToString("dd/MM/yyyy");
                    objlistadescuento.horaInicial = Convert.ToString(dr["horaInicial"]);
                    objlistadescuento.horaFin = Convert.ToString(dr["horaFin"]);
                    objlistadescuento.descuento = Convert.ToDecimal(dr["porcentaje"]);
                    objlistadescuento.codEstado = Convert.ToInt32(dr["codEstado"]);
                    objlistadescuento.desEstado = Convert.ToString(dr["desEstado"]);
                    objlistadescuento.codMarca = Convert.ToInt32(dr["codMarca"]);
                    objlistadescuento.desMarca = Convert.ToString(dr["desMarca"]);
                    objlistadescuento.codTipo = Convert.ToInt32(dr["codTipo"]);
                    objlistadescuento.desTipo = Convert.ToString(dr["desTipo"]);
                    objlistadescuento.codBanco = Convert.ToString(dr["codBanco"]);
                    objlistadescuento.abrBanco = Convert.ToString(dr["abrBanco"]);
                    objlistadescuento.desBanco = Convert.ToString(dr["desBanco"]);
                    objlistadescuento.codCategoria = Convert.ToInt32(dr["codCategoria"]);
                    objlistadescuento.descCategoria = Convert.ToString(dr["descCategoria"]);

                    listaDeatlle.Add(objlistadescuento);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDeatlle;
        }

        public CENMensajeDescuento cambiarEstado(int idDesc, int estado)
        {
            CENMensajeDescuento mensajEstado = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();
            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_descuento_activar_desactivar", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("ntraDesc", MySqlDbType.Int32).Value = idDesc;
                cmd.Parameters.Add("codEstado", MySqlDbType.Int32).Value = estado;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    mensajEstado = new CENMensajeDescuento();
                    mensajEstado.flag = Convert.ToInt32(dr["flag"]);
                    mensajEstado.mensaje = Convert.ToString(dr["mensaje"]);
                    mensajEstado.ntraDescuento = Convert.ToInt32(dr["codigoDesc"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mensajEstado;
        }

        public DateTime ConvertFechaStringToDate(string fecha)
        {
            try
            {
                CultureInfo MyCultureInfo = new CultureInfo(CENConstante.g_const_es_PE);
                DateTime myDate = DateTime.ParseExact(fecha, CENConstante.g_const_formfech, MyCultureInfo);
                return myDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int eliminarDescuento(int ntraDescuentos)
        {
            int ok = 0;
            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();
            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_descuento_eliminar_descuento", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("ntraDesc", MySqlDbType.Int32).Value = ntraDescuentos;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ok = Convert.ToInt32(dr["flag"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
    }
}
