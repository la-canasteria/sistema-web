﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADBancos
    {

        public List<CENBancos> ListarBancos(int flag)
        {
            List<CENBancos> listabanco = new List<CENBancos>();
            CENBancos objBancos = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("flag", MySqlDbType.Int32).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    objBancos = new CENBancos();
                    objBancos.codigo = Convert.ToString(dr["codBanco"]);
                    objBancos.abreviatura = Convert.ToString(dr["abreviatura"]);
                    objBancos.descripcion = Convert.ToString(dr["descripcion"]);
                    listabanco.Add(objBancos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listabanco;
        }
    }
}
