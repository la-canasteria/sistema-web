﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using MySql.Data.MySqlClient;

namespace CAD
{
    public class CADUsuario
    {
        public CENDatosUsuario DatosUsuarios(string usuario, string contraseña, int intentos)
        //DESCRPCION: Obtiene las credenciales de usuario
        {
            MySqlConnection con = null;
            MySqlCommand cmd;
            MySqlDataReader dr;
            CENDatosUsuario objUsuarios = new CENDatosUsuario();
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_verificar_user", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("nomUser", MySqlDbType.VarChar).Value = usuario;
                cmd.Parameters.Add("pass", MySqlDbType.VarChar).Value = contraseña;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objUsuarios.codRespuesta = Convert.ToInt32(dr["respuesta"].ToString());
                    objUsuarios.descRespuesta = dr["mensaje"].ToString();
                    objUsuarios.perfil = dr["perfil"].ToString();
                    objUsuarios.codUsuario = Convert.ToInt32(dr["ntraUsuario"].ToString());
                    objUsuarios.codPersona = Convert.ToInt32(dr["fkcodPersona"].ToString());
                    objUsuarios.nombre = dr["nombre"].ToString();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objUsuarios;
        }

    }
}
