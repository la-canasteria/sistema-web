﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADMarca
    {
        public List<CENConcepto> listarMarca(int flag)
        {
            List<CENConcepto> listaconcepto = new List<CENConcepto>();
            CENConcepto objConcepto = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("flag", MySqlDbType.Int32).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    objConcepto = new CENConcepto();
                    objConcepto.codigo = Convert.ToInt32(dr["codigo"]);
                    objConcepto.descripcion = Convert.ToString(dr["descripcion"]);
                    listaconcepto.Add(objConcepto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaconcepto;
        }
    }
}
