﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADReporteVentasDescuentos
    {
        private MySqlConnection conexion;
        public List<CENReporteVentasDescuentos> ReporteVentasDescuentos(string fechin, string fechfin, string banco, int categoria)
        {

            MySqlDataReader dr = null;
            CADConexionMySQL conex = new CADConexionMySQL();
            List<CENReporteVentasDescuentos> listareporte = new List<CENReporteVentasDescuentos>();

            conexion = new MySqlConnection(conex.CxMySQL());
            conexion.Open();
            try
            {

                //baja Categoria
                using (MySqlCommand command2 = new MySqlCommand("pa_dbcanasteria_vt_mostrar_ventas", conexion))
                {
                    string fechai = "";
                    string fechaf = "";

                    if (String.IsNullOrEmpty(fechin))
                    {
                        fechai = "";
                    }
                    else
                    {
                        fechai = Convert.ToDateTime(fechin).ToString("yyyy/MM/dd");
                    }

                    if (String.IsNullOrEmpty(fechfin))
                    {
                        fechaf = "";
                    }
                    else
                    {
                        fechaf = Convert.ToDateTime(fechfin).ToString("yyyy/MM/dd");
                    }

                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.Add("@p_fechain", MySqlDbType.VarChar).Value = fechai;
                    command2.Parameters.Add("@p_fechafi", MySqlDbType.VarChar).Value = fechaf;
                    command2.Parameters.Add("@p_banco", MySqlDbType.String).Value = banco;
                    command2.Parameters.Add("@p_categoria", MySqlDbType.String).Value = categoria;
                    command2.CommandTimeout = 0;
                    dr = command2.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CENReporteVentasDescuentos cenreporte = new CENReporteVentasDescuentos();
                            cenreporte.codvent = Convert.ToInt32(dr["codvent"].ToString());
                            cenreporte.fechped = Convert.ToDateTime(dr["fechped"]).ToString("dd/MM/yyyy");
                            cenreporte.desarti = dr["desarti"].ToString();
                            cenreporte.categoria = dr["categoria"].ToString();
                            cenreporte.descbanco = dr["descbanco"].ToString();
                            cenreporte.tarjeta = dr["tarjeta"].ToString();
                            cenreporte.cantive = Convert.ToInt32(dr["cantive"].ToString());
                            cenreporte.importe = Convert.ToDecimal(dr["importe"].ToString());
                            cenreporte.impdscto = Convert.ToDecimal(dr["impdscto"].ToString());
                            cenreporte.impvent = Convert.ToDecimal(dr["impvent"].ToString());
                            listareporte.Add(cenreporte);
                        }
                    }
                }
                return listareporte;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

        }

        public DateTime ConvertFechaStringToDate(string fecha)
        {
            try
            {
                CultureInfo MyCultureInfo = new CultureInfo(CENConstante.g_const_es_PE);
                DateTime myDate = DateTime.ParseExact(fecha, CENConstante.g_const_formfech, MyCultureInfo);
                return myDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
