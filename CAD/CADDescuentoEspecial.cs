﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADDescuentoEspecial
    {
        public ResponseDescuentoEspecial grabarDescuentoEspecial(RequestRegDescEspecial datos, string usuario)
        {
            ResponseDescuentoEspecial mensajeRegistrar = new ResponseDescuentoEspecial();

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            TimeSpan horaI = new TimeSpan();
            TimeSpan horaF = new TimeSpan();

            try
            {
                horaI = TimeSpan.Parse(datos.horaInicial);
                horaF = TimeSpan.Parse(datos.horaFin);

                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_registrar_descuento_especial", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("codDescuento", MySqlDbType.VarChar, 20).Value = datos.codDescuento;
                cmd.Parameters.Add("descripcion", MySqlDbType.VarChar, 200).Value = datos.descripcion;
                cmd.Parameters.Add("fechaInicial", MySqlDbType.Date).Value = ConvertFechaStringToDate(datos.fechaInicial);
                cmd.Parameters.Add("fechaFin", MySqlDbType.Date).Value = ConvertFechaStringToDate(datos.fechaFin);
                cmd.Parameters.Add("horaInicial", MySqlDbType.Time).Value = horaI;
                cmd.Parameters.Add("horaFin", MySqlDbType.Time).Value = horaF;
                cmd.Parameters.Add("flagestado", MySqlDbType.Int32).Value = datos.flagestado;
                cmd.Parameters.Add("tipoTarjeta", MySqlDbType.Int32).Value = datos.tipoTarjeta;
                cmd.Parameters.Add("codBanco", MySqlDbType.VarChar).Value = datos.codBanco;
                cmd.Parameters.Add("codCategoria", MySqlDbType.Int32).Value = datos.codCategoria;
                cmd.Parameters.Add("usuario", MySqlDbType.VarChar).Value = usuario;
                cmd.Parameters.Add("cod_tarjeta_xml", MySqlDbType.Text).Value = datos.cod_tarjeta;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    mensajeRegistrar.codError = Convert.ToInt32(dr["codigo"]);
                    mensajeRegistrar.descError = Convert.ToString(dr["mensaje"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mensajeRegistrar;
        }

        public List<ResponseBuscarDescuentoEspecial> buscarDescuento(int categoria, string banco)
        {
            List<ResponseBuscarDescuentoEspecial> lista = new List<ResponseBuscarDescuentoEspecial>();

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_descuento_especial", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("categoria", MySqlDbType.Int32).Value = categoria;
                cmd.Parameters.Add("banco", MySqlDbType.VarChar).Value = banco;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ResponseBuscarDescuentoEspecial item = new ResponseBuscarDescuentoEspecial();
                    item.codDescuento = Convert.ToInt32(dr["ntraDescuento"]);
                    item.codCategoria = Convert.ToInt32(dr["categoria"]);
                    item.descCategoria = Convert.ToString(dr["descCategoria"]);
                    item.codBanco = Convert.ToString(dr["banco"]);
                    item.descBanco = Convert.ToString(dr["descBanco"]);
                    item.codTarjeta = Convert.ToInt32(dr["tipoTarjeta"]);
                    item.descTarjeta = Convert.ToString(dr["descTipoTarjeta"]);
                    item.fechai = Convert.ToDateTime(dr["fechaInicial"]).ToString("dd/MM/yyyy");
                    item.fechaf = Convert.ToDateTime(dr["fechaFin"]).ToString("dd/MM/yyyy");
                    item.horai = Convert.ToString(dr["horaInicial"]);
                    item.horaf = Convert.ToString(dr["horaFin"]);
                    item.codEstado = Convert.ToInt32(dr["estado"]);
                    item.descEstado = Convert.ToString(dr["descEstado"]);
                    item.tiposTarjetas = Convert.ToString(dr["tiposTarjetas"]);
                    lista.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return lista;
        }

        public DateTime ConvertFechaStringToDate(string fecha)
        {
            try
            {
                CultureInfo MyCultureInfo = new CultureInfo(CENConstante.g_const_es_PE);
                DateTime myDate = DateTime.ParseExact(fecha, CENConstante.g_const_formfech, MyCultureInfo);
                return myDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
