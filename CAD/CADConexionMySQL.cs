﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;
using MySql.Data.MySqlClient;

namespace CAD
{
    public class CADConexionMySQL
    {
        public String CxMySQL()
        {
            //DESCRIPCION : CONEXION CON SQL SERVER
            try
            {
                return ConfigurationManager.ConnectionStrings[CENConstante.ConexionSQL].ConnectionString.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
