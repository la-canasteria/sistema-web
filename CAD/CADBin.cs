﻿using CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CADBin
    {
        public List<CENBinLista> ListarBIN(CENBin datos)
        {
            List<CENBinLista> list_bin = new List<CENBinLista>();
            CENBinLista objBinLista = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_bin_listar", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("codMarca", MySqlDbType.Int16).Value = datos.codMarca;
                cmd.Parameters.Add("codBanco", MySqlDbType.VarChar).Value = datos.codBanco;
                cmd.Parameters.Add("codTipo", MySqlDbType.Int16).Value = datos.codTipo;


                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objBinLista = new CENBinLista();
                    objBinLista.id = Convert.ToInt32(dr["ntraParametrizador"]);
                    objBinLista.BIN = Convert.ToString(dr["codBIN"]);
                    objBinLista.idMarca = Convert.ToInt32(dr["idMarca"]);
                    objBinLista.descMarca = Convert.ToString(dr["descMarca"]);
                    objBinLista.codBanco = Convert.ToString(dr["codBanco"]);
                    objBinLista.descBanco = Convert.ToString(dr["descBanco"]);
                    objBinLista.idTipo = Convert.ToInt32(dr["codTarjeta"]);
                    objBinLista.descTipo = Convert.ToString(dr["descTarjeta"]);
                    objBinLista.marcaBaja = Convert.ToInt32(dr["marcaBaja"]);

                    list_bin.Add(objBinLista);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            finally
            {
                con.Close();
            }
            return list_bin;
        }
        
        public CENResponseStoreProcedure InsertarBIN(CENBinInsert objBin)
        {
            CENResponseStoreProcedure objRespuesta = new CENResponseStoreProcedure();

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();
            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_registrar_BIN", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("codBIN", objBin.codBINInsert);
                cmd.Parameters.AddWithValue("marca", objBin.codMarcaInsert);
                cmd.Parameters.AddWithValue("tarjeta", objBin.codTipoInsert);
                cmd.Parameters.AddWithValue("banco", objBin.codBancoInsert);
                cmd.Parameters.AddWithValue("usuario", objBin.usuarioInsert);

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objRespuesta.codRespuesta = Convert.ToInt16(dr["codRespuesta"]);
                    objRespuesta.descRespuesta = Convert.ToString(dr["descRespuesta"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objRespuesta;
        }
        
        public int ElimiarBIN(CENBinLista objBinAD)
        {
            int response = 0;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();
            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_eliminar_BIN", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("id", objBinAD.id);
                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }
        
        public int ActualizarBIN(CENBinActualizar objBIN, int id)
        {
            int response = 0;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_actualizar_bin", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("bin", objBIN.codBINActualizar);

                cmd.Parameters.AddWithValue("id", id);
                con.Open();

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }
        
        public int existeBin(int codm, int ttar, string codb)
        {
            int response = 1;
            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();
            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_existe_bin", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("codm", MySqlDbType.Int16).Value = codm;
                cmd.Parameters.Add("ttar", MySqlDbType.Int16).Value = ttar;
                cmd.Parameters.Add("codb", MySqlDbType.VarChar).Value = codb;

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToInt16(dr["l_cant"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }
    }
}
