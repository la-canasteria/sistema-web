﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using MySql.Data.MySqlClient;

namespace CAD
{
    public class CADConcepto
    {
        public List<CENConcepto> ListarCoceptos(int flag)
        {
            List<CENConcepto> listaconcepto = new List<CENConcepto>();
            CENConcepto objConcepto = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("flag", MySqlDbType.Int32).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    objConcepto = new CENConcepto();
                    objConcepto.codigo = Convert.ToInt32(dr["codigo"]);
                    objConcepto.descripcion = Convert.ToString(dr["descripcion"]);
                    listaconcepto.Add(objConcepto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaconcepto;
        }
        
        public List<CENConcepto> ListarTipo(String banco)
        {
            List<CENConcepto> listaconcepto = new List<CENConcepto>();
            CENConcepto objConcepto = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_tipos", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("p_banco", MySqlDbType.VarChar).Value = banco;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    objConcepto = new CENConcepto();
                    objConcepto.codigo = Convert.ToInt32(dr["codigo"]);
                    objConcepto.descripcion = Convert.ToString(dr["descripcion"]);
                    listaconcepto.Add(objConcepto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaconcepto;
        }

        public List<CENBanco> ListarBanco(int flag)
        {
            List<CENBanco> listabanco = new List<CENBanco>();
            CENBanco objBanco = null;

            MySqlConnection con = null;
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;
            CADConexionMySQL CadCx = new CADConexionMySQL();

            try
            {
                con = new MySqlConnection(CadCx.CxMySQL());
                cmd = new MySqlCommand("pa_listar_datos_select_x_flag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("flag", MySqlDbType.Int32).Value = flag;
                con.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    objBanco = new CENBanco();
                    objBanco.codigo = Convert.ToString(dr["codBanco"]);
                    objBanco.abreviatura = Convert.ToString(dr["abreviatura"]);
                    objBanco.descripcion = Convert.ToString(dr["descripcion"]);
                    listabanco.Add(objBanco);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listabanco;
        }

    }
}
