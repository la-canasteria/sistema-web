﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENConcepto
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }

    public class CENBanco
    {
        public string codigo { get; set; }
        public string abreviatura { get; set; }
        public string descripcion { get; set; }
    }

}
