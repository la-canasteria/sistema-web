﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENReporteVentasDescuentos
    {
        public int codvent { get; set; }
        public string fechped { get; set; }
        public string desarti { get; set; }
        public string categoria { get; set; }
        public string descbanco { get; set; }
        public string tarjeta { get; set; }
        public int cantive { get; set; }
        public decimal importe { get; set; }
        public decimal impdscto { get; set; }
        public decimal impvent { get; set; }
    }
}
