﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENBin
    {

        public int codMarca { get; set; }
        public string codBanco { get; set; }
        public int codTipo { get; set; }


        public CENBin( int codMarca, string codBanco, int codTipo)
        {
            this.codMarca = codMarca;
            this.codBanco = codBanco;
            this.codTipo = codTipo;
        }

    }

    public class CENBinInsert
    {
        public string codBINInsert { get; set; }
        public int codMarcaInsert { get; set; }
        public int codTipoInsert { get; set; }
        public string codBancoInsert { get; set; }
        public string usuarioInsert { get; set; }
    }


    public class CENBinActualizar
    {
        public int idActualizar { get; set; }
        public string codBINActualizar { get; set; }
    }

    public class CENBinLista
    {
        public int id { get; set; }
        public string BIN { get; set; }
        public int idMarca { get; set; }
        public string descMarca { get; set; }
        public int idTipo { get; set; }
        public string descTipo { get; set; }
        public string codBanco { get; set; }
        public string descBanco { get; set; }
        public int marcaBaja { get; set; }

        public CENBinLista()
        {

        }
    }
}
