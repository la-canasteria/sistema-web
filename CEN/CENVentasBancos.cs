﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENVentasBancos
    {
        public string descbanco { get; set; }
        public string tarjeta { get; set; }
        public decimal impdscto { get; set; }
        public int cantive { get; set; }
        public decimal imptven { get; set; }
    }
}
