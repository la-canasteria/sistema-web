﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENRequestErrorWeb
    {
        public string sistema { get; set; }
        public string modulo { get; set; }
        public string opcion { get; set; }
        public Int16 tipo { get; set; }
        public string descripcion { get; set; }
        public string usuario { get; set; }
    }
}
