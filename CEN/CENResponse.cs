﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENResponseStoreProcedure
    {
        public int codRespuesta { get; set; }
        public string descRespuesta { get; set; }
    }

    public class CENResponse
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }
    }

    public class ResponseBuscarDescuentoEspecial
    {
        public int codDescuento { get; set; }
        public int codCategoria { get; set; }
        public string descCategoria { get; set; }
        public string codBanco { get; set; }
        public string descBanco { get; set; }
        public int codTarjeta { get; set; }
        public string descTarjeta { get; set; }
        public string fechai { get; set; }
        public string fechaf { get; set; }
        public string horai { get; set; }
        public string horaf { get; set; }
        public int codEstado { get; set; }
        public string descEstado { get; set; }
        public string tiposTarjetas { get; set; }
    }
}
