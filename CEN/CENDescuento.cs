﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CENMensajeDescuento
    {
        public int flag { get; set; }
        public string mensaje { get; set; }
        public int ntraDescuento { get; set; }

    }

    public class CENRegistrarDescuento
    {
        public int proceso { get; set; }
        public string descripcion { get; set; }
        public string fechaVigenciaI { get; set; }
        public string fechaVigenciaF { get; set; }
        public string horaI { get; set; }
        public string horaF { get; set; }
        public int flagestado { get; set; }
        public int codMarca { get; set; }
        public int CodTipo { get; set; }
        public string CodBanco { get; set; }
        public string CodCategoria { get; set; }
        public decimal codCantidad { get; set; }
        public int ntraDescuento { get; set; }
        public string nomUsuario { get; set; }
    }

    public class CENListarDescuento
    {
        public int ntraDescuento { get; set; }  //numero id del descuento
        public string descripcion { get; set; } //descripcion del descuento
        public string fechaInicial { get; set; }    //fecha de inicio
        public string fechaFin { get; set; }    //fecha de fin
        public string horaInicial { get; set; }     //hora inicio
        public string horaFin { get; set; }     //hora fin
        public decimal descuento { get; set; }  //monto del descuento
        public int codCategoria { get; set; }
        public string descCategoria { get; set; }
        public int codEstado { get; set; }  //codigo estado del descuento
        public string desEstado { get; set; } //descripcion del estado
        public int codMarca { get; set; }  //codigo de la marca de tarjeta
        public string desMarca { get; set; } //descripcion de la marca de tarjeta
        public int codTipo { get; set; }  //codigo del tipo de tarjeta
        public string desTipo { get; set; } //descripcion del tipo de tarjeta
        public string codBanco { get; set; } //codigo del banco
        public string abrBanco { get; set; } //abreviatura del banco
        public string desBanco { get; set; } //descripcion del banco
        
    }

}
