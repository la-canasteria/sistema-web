﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class ResponseDescuentoEspecial
    {
        public int codError { get; set; }
        public string descError { get; set; }
    }

    public class RequestRegDescEspecial
    {
        public string codDescuento { get; set; }
        public string descripcion { get; set; }
        public string fechaInicial { get; set; }
        public string fechaFin { get; set; }
        public string horaInicial { get; set; }
        public string horaFin { get; set; }
        public int flagestado { get; set; }
        public int tipoTarjeta { get; set; }
        public string codBanco { get; set; }
        public int codCategoria { get; set; }
        public string cod_tarjeta { get; set; }
    }
}
