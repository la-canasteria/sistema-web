﻿<%@ Page ClientIDMode="Static" Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantDescuento.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantDetalleDescuento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>MANTENEDOR DE DESCUENTO</title>
    <!-- LIBRERIAS EXPORTAR EN EXCEL -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="Scripts/frmMantDescuento.js"></script>
    <style>
        body{
            background-image:none;
            background-color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="title_frm col-xl-12">
            MANTENEDOR DE DESCUENTO
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Filtros de Busqueda
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="id_marca">Marca Tarjeta</label>
                            </div>
                            <select class="custom-select" id="id_marca" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="id_banco">Banco</label>
                            </div>
                            <select class="custom-select" id="id_banco" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="id_tipo">Tipo de Tarjeta</label>
                            </div>
                            <select class="custom-select" id="id_tipo" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="id_estado">Estado</label>
                            </div>
                            <select class="custom-select" id="id_estado" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="id_categoria">Categoria</label>
                            </div>
                            <select class="custom-select" id="id_categoria" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                </div>
                <%-- Rango de Fechas --%>
                <div class="row">
                    <div class="col-6 ml-3">
                        <label class="label">Fecha Vig.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group date col-md-4 mb-3" id="datetimepicker1" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" id="id_fechaI" name="id_fechaI" placeholder="F. Inicio" />
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="input-group date col-md-4 mb-3" id="datetimepicker2" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" id="id_fechaF" name="id_fechaF" placeholder="F. Fin" />
                        <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                            <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <button type="button" class="btn btn-primary" id="btnBuscar" >BUSCAR</button>
                    </div>
                </div>
                <!-- div para mostrar mensaje de validacion de fecha de vigencia-->
                <div class="row form-group col-xs-12" >
                    <div class="col-xs-5 form-group" id="msjFecha" hidden="">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />

    <div class="table-responsive">
        <table id="id_tblDescuento" class="table table-primary table-bordered table-hover nowrap">
            <thead>
                <tr class="bg-primary">
                    <th>N° DESC</th>    <!-- 0 -->
                    <th>MARCA</th>
                    <th>TIPO</th>
                    <th>BANCO</th>
                    <th>CATEGORIA</th>
                    <th>DESC (%)</th> 
                    <th>ESTADO</th>  <!-- 6 -->
                    <th>FECHA I</th>
                    <th>FECHA F</th>
                    <th>HORA I</th>
                    <th>HORA F</th>  
                    <th>ACCION</th>  <!-- 11 -->
                    <th>CODEST</th>
                    <th>CODMARCA</th>   
                    <th>CODTIPO</th>
                    <th>CODBANCO</th>
                    <th>CODCATEGORIA</th>
                </tr>
            </thead>
            <tbody id="tbl_body_table" class="ui-sortable">
                        
            </tbody>
        </table>
    </div>
    <br />
    <button type="button" class="btn btn-primary" id="btnAgregar" data-toggle="modal" data-target="#modalDescuento">AGREGAR</button>
    <br />
    <br />

        <!-- Ventana modal para Registrar descuento -->
    <div class="modal fade" id="modalDescuento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class=" modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">REGISTRAR DESCUENTO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <!-- Datos Generales -->
                        <div class="form-group">
                            <h4 class="box-filtros">Datos Generales</h4>
                        </div>

                        <!-- Fechas -->
                        <div class="form-group">
                            <!-- div para mostrar mensaje de validacion de fecha de vigencia-->
                            <div class="row form-group col-xs-12" >
                                <div class="col-xs-8 form-group" id="msjFechaEdit" hidden="">

                                </div>
                            </div>
                            <div class="form-row">
                                <label class="label">Fecha Vig.</label>
                            </div>
                            <div class="form-row">
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker3" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3" id="id_fecha_vigI" name="id_fecha_vigI" placeholder="F. Inicio" />
                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker4" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4" id="id_fecha_vigF" name="id_fecha_vigF" placeholder="F. Fin" />
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Horas -->
                        <div class="form-group">
                            <div class="form-row">
                                <label class="label">Hora</label>
                            </div>
                            <div class="form-row">
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker5" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker5" id="id_horaI" name="id_horaI" placeholder="H. Inicio" />
                                    <div class="input-group-append" data-target="#datetimepicker5" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                </div>
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker6" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker6" id="id_horaF" name="id_horaF" placeholder="H. Inicio" />
                                    <div class="input-group-append" data-target="#datetimepicker6" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Activar -->
                        <div class="form-group">
                            <label class="label label-default mr-2 ">Activar</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="activacion" id="id_activado">
                              <label class="form-check-label" for="inlineRadio1">SI</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="activacion" id="id_desactivado">
                              <label class="form-check-label" for="inlineRadio2">NO</label>
                            </div>
                        </div>

                        <!-- Datos del Descuento -->
                        <div class="form-group">
                            <h4 class="box-filtros">Datos del Descuento</h4>
                        </div>

                        <!-- Marca -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="M_marca">Marca Tarjeta</label>
                                </div>
                                <select class="custom-select" id="M_marca" name="M_marca">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                            <%--<label class="col-xs-2 label label-default horizontal">Marca Tarjeta</label>
                            <select class="form-control horizontal cl_marca" id="M_marca">
                                <option value = "0"> -Seleccionar- </option>
                            </select>--%>
                        </div>

                        <!-- Banco -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="M_banco">Banco</label>
                                </div>
                                <select class="custom-select" id="M_banco" name="M_banco">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                            <%--<label class="col-xs-2 label label-default horizontal">Banco</label>
                            <select class="form-control horizontal cl_banco" id="M_banco">
                                <option value = "0"> -Seleccionar- </option>
                            </select>--%>
                        </div>
                        
                        <!-- Tipo -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="M_tipo">Tipo Tarjeta</label>
                                </div>
                                <select class="custom-select" id="M_tipo" name="M_tipo">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                            <%--<label class="col-xs-2 label label-default horizontal">Tipo Tarjeta</label>
                            <select class="form-control horizontal cl_tipo" id="M_tipo">
                                <option value = "0"> -Seleccionar- </option>
                            </select>--%>
                        </div>
                        
                        <!-- Categoria -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="M_categoria">Categoria</label>
                                </div>
                                <select class="custom-select" id="M_categoria" name="M_categoria">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                            <%--<label class="col-xs-2 label label-default horizontal">Categoria</label>
                            <select class="form-control horizontal cl_categoria" id="M_categoria">
                                <option value = "0"> -Seleccionar- </option>
                            </select>--%>
                        </div>
                        
                        <!-- Porcentaje -->
                        <div class="form-group">
                            <div class="input-group flex-nowrap">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="addon-wrapping">Porcentaje</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Username" aria-describedby="addon-wrapping" id="id_cantidad">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                            <%--<label class="col-xs-2 label label-default horizontal">Porcentaje</label>
                            <div class="col-xs-4 input-group">
                                <input type="number" step="0.01" class="form-control " id="id_cantidad" placeholder="Ingrese Porcentaje"/>
                                <div class="input-group-addon">%</div>
                            </div>--%>
                        </div>
                        
                        <!-- Footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btncancelar">CANCELAR</button>
                            <button type="submit" class="btn btn-primary" id="btnregistrar">REGISTRAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
