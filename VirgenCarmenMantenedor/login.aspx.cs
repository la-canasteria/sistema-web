﻿using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<CENUsuario> getSession()
        {
            List<CENUsuario> DataUser = null;
            CENUsuario user = new CENUsuario();
            try
            {
                DataUser = new List<CENUsuario>();
                user.perfil = System.Web.HttpContext.Current.Session["perfil"].ToString();
                user.nombre = System.Web.HttpContext.Current.Session["nombres"].ToString();
                DataUser.Add(user);
            }
            catch (Exception ex)
            {
                CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
                objCENRequestErrorWeb.modulo = "login.aspx.cs => getSession";
                objCENRequestErrorWeb.opcion = "Datos: sin datos de entrada";
                objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
                objCENRequestErrorWeb.tipo = 1;
                objCENRequestErrorWeb.usuario = HttpContext.Current.Session["codUser"].ToString();
                objCENRequestErrorWeb.descripcion =
                        "Message: " + ex.Message + "\n" +
                        "Source: " + ex.Source;
                new CLNError().registrarLog(objCENRequestErrorWeb);
            }
            return DataUser;
        }

        [WebMethod]
        public static Boolean InsertSession(string perfil, string nombre, string sucursal, string codUser, string codPersona, string usuario)
        {
            try
            {
                System.Web.HttpContext.Current.Session["perfil"] = perfil;
                System.Web.HttpContext.Current.Session["nombres"] = nombre;
                System.Web.HttpContext.Current.Session["sucursal"] = sucursal;
                System.Web.HttpContext.Current.Session["codUser"] = codUser;
                System.Web.HttpContext.Current.Session["codPersona"] = codPersona;
                System.Web.HttpContext.Current.Session["usuario"] = usuario;
                
            }
            catch (Exception ex)
            {
                CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
                objCENRequestErrorWeb.modulo = "login.aspx.cs => InsertSession";
                objCENRequestErrorWeb.opcion = "Datos: " + perfil + " | " + nombre + " | " + sucursal + " | " + codUser + " | " + codPersona;
                objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
                objCENRequestErrorWeb.tipo = 1;
                objCENRequestErrorWeb.usuario = codUser;
                objCENRequestErrorWeb.descripcion =
                        "Message: " + ex.Message + "\n" +
                        "Source: " + ex.Source;
                new CLNError().registrarLog(objCENRequestErrorWeb);
            }
            return true;
        }

        [WebMethod]
        public static CENResponse consultarDatos(string user, string pass, int intentos)
        {
            CENResponse datos = new CENResponse();
            datos.codigo = -1;
            datos.descripcion = "ACCESO NO AUTORIZADO";

            try
            {
                CENDatosUsuario datosUsuario = new CLNUsuario().credencialesUsuario(user, pass, intentos);
                if (datosUsuario.codRespuesta == 0) 
                {
                    System.Web.HttpContext.Current.Session["perfil"] = datosUsuario.perfil;
                    System.Web.HttpContext.Current.Session["nombres"] = datosUsuario.nombre;
                    System.Web.HttpContext.Current.Session["sucursal"] = "";
                    System.Web.HttpContext.Current.Session["codUser"] = datosUsuario.codUsuario;
                    System.Web.HttpContext.Current.Session["codPersona"] = datosUsuario.codPersona;
                    System.Web.HttpContext.Current.Session["usuario"] = user;
                    datos.codigo = datosUsuario.codRespuesta;
                    datos.descripcion = datosUsuario.descRespuesta;
                }
            }
            catch (Exception ex)
            {
                datos.codigo = -2;
                datos.descripcion = ex.Message;

                CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
                objCENRequestErrorWeb.modulo = "login.aspx.cs => consultarDatos";
                objCENRequestErrorWeb.opcion = "Datos: " + user + " | " + pass + " | " + intentos;
                objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
                objCENRequestErrorWeb.tipo = 1;
                objCENRequestErrorWeb.usuario = user;
                objCENRequestErrorWeb.descripcion =
                        "Message: " + ex.Message + "\n" +
                        "Source: " + ex.Source;
                new CLNError().registrarLog(objCENRequestErrorWeb);
            }
            return datos;
        }

        [WebMethod]
        public static List<CENUsuario> CerrarSesion()
        {
            List<CENUsuario> datos = new List<CENUsuario>();
            CENUsuario dato = new CENUsuario();
            try
            {
                dato.mensaje = CENConstante.g_msj_sesion_cerrada;
                dato.respuesta = CENConstante.g_const_2000;
                datos.Add(dato);
                System.Web.HttpContext.Current.Session["codUser"] = null;
            }
            catch (Exception ex)
            {
                CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
                objCENRequestErrorWeb.modulo = "login.aspx.cs => CerrarSesion";
                objCENRequestErrorWeb.opcion = "Datos: sin datos de entrada";
                objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
                objCENRequestErrorWeb.tipo = 1;
                objCENRequestErrorWeb.usuario = HttpContext.Current.Session["codUser"].ToString();
                objCENRequestErrorWeb.descripcion =
                        "Message: " + ex.Message + "\n" +
                        "Source: " + ex.Source;
                new CLNError().registrarLog(objCENRequestErrorWeb);
            }
            return datos;
        }

        [WebMethod]
        public static string ObtenerUser()
        {
            string duser = CENConstante.g_const_vacio;
            try
            {
                duser = System.Web.HttpContext.Current.Session["nombres"].ToString();
            }
            catch (Exception ex)
            {
                CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
                objCENRequestErrorWeb.modulo = "login.aspx.cs => ObtenerUser";
                objCENRequestErrorWeb.opcion = "Datos: sin datos de entrada";
                objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
                objCENRequestErrorWeb.tipo = 1;
                objCENRequestErrorWeb.usuario = HttpContext.Current.Session["codUser"].ToString();
                objCENRequestErrorWeb.descripcion =
                        "Message: " + ex.Message + "\n" +
                        "Source: " + ex.Source;
                new CLNError().registrarLog(objCENRequestErrorWeb);
            }

            return duser;
        }
    }
}