﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class ReporteVentasBancos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarBancos();
        }
        public void cargarBancos()
        {
            try
            {
                List<CENBancos> listBancos = new List<CENBancos>();
                listBancos = new CADBancos().ListarBancos(4);
                cbo_bancos.Items.Clear();
                cbo_bancos.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemEmpresa in listBancos)
                {
                    string codigo = itemEmpresa.codigo.ToString();
                    string nombre = itemEmpresa.descripcion;
                    cbo_bancos.Items.Add(new ListItem(nombre, codigo));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        [WebMethod]
        public static List<CENVentasBancos> listaventasBancos(string fechin, string fechfin, string banco)
        {
            List<CENVentasBancos> listaventas = null;
            CADVentasBancos objcadReporte = null;

            try
            {
                objcadReporte = new CADVentasBancos();
                listaventas = objcadReporte.CADReporteVentasBancos(fechin, fechfin, banco);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return listaventas;

        }
    }
}