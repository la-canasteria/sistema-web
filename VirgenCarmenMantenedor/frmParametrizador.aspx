﻿<%@ Page ClientIDMode="Static" Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmParametrizador.aspx.cs" Inherits="VirgenCarmenMantenedor.frmParametrizador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Parametrizador BIN</title>
    
    <!-- LIBRERIAS EXPORTAR EN EXCEL -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

    <script src="Scripts/parametrizador_ajax.js"></script>
    <style>
        body{
            background-image:none;
            background-color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="title_frm col-xl-12">
            MANTENEDOR DE CODIGO BIN
        </div>
    </div>

    <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Filtros de Busqueda
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-xl-4 mb-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_banco">Marca</label>
                                </div>
                                <select class="custom-select" id="marca" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-4 mb-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_categoria">Banco</label>
                                </div>
                                <select class="custom-select" id="banco" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-4 mb-2">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_banco">Tipo Tarjeta</label>
                                </div>
                                <select class="custom-select" id="tipoTarjeta" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-4">
                            <button type="button" class="btn btn-primary" id="id_btnBuscar" >BUSCAR</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <br />
    <br />
    <div class="table-responsive">
        <table id="tb_BIN" class="table table-primary table-bordered table-hover nowrap">
            <thead>
                <tr class="bg-primary">
                    <th scope="col">id</th>
                    <th scope="col">CODIGO BIN</th>
                    <th scope="col">idMarca</th>
                    <th scope="col">MARCA</th>
                    <th scope="col">idTipo</th>
                    <th scope="col">TIPO DE TARJETA</th>
                    <th scope="col">idBanco</th>
                    <th scope="col">BANCO</th>
                    <th scope="col">marcaBaja</th>
                    <th scope="col">ACCION</th>
                </tr>
            </thead>
            <tbody id="tbl_body_table">
                    
            </tbody>
        </table>    
    </div>
    <br />
    <button type="button" class="btn btn-primary" id="buttonModal" data-toggle="modal" data-target="#modalRegBIN" >AGREGAR</button>
    <br />
    <br />

    <!-- Modal Registrar Codigo BIN -->
    <div class="modal fade" id="modalRegBIN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelReg">Mantenedor de Código BIN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Cod. BIN</label>
                                <input type="number"  class="form-control" id="cod_BIN_reg"  min="0" max="999999"/>
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="inputName">Marca</label>
                                <select class="form-control" id="marca_reg" runat="server">
                                    <option value="0">SELECCIONAR</option>                                   
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Banco</label>
                                <select class="form-control" id="banco_reg" runat="server">
                                    <option value="0">SELECCIONAR</option>                                   
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="inputName">Tipo Trajeta</label>
                                <select class="form-control" id="tipoTarjeta_reg">
                                    <option value="0">SELECCIONAR</option>                                   
                                </select>
                            </div>
                        </div>

                        <div class="form-group" style="text-align: right">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelarR">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnRegistrar">Registrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


            <!-- Modal Modificar Codigo BIN -->
    <div class="modal fade" id="modalModBIN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Cod. BIN</label>
                                <input type="number"  class="form-control" id="cod_BIN_mod" min="0" max="999999"/>
                                <input type="hidden"  class="form-control" id="cod_registro_mod" />
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="inputName">Marca</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_marca_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="marca_mod" />
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Tipo Trajeta</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_tipoTrajeta_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="tipoTrajeta_mod" />
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="inputName">Banco</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_banco_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="banco_mod" />
                            </div>
                        </div>

                        <div class="form-group" style="text-align: right">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnActualizar">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
