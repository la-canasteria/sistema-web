﻿using CAD;
using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantDetalleDescuento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<CENConcepto> ListarTipo(String banco)
        {
            List<CENConcepto> listaconcepto = null;
            CLNConcepto objCLNConcepto = null;
            try
            {
                objCLNConcepto = new CLNConcepto();
                listaconcepto = objCLNConcepto.ListarTipo(banco);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = banco;
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "ListarTipo", datosLog, ex);
            }

            return listaconcepto;
        }

        [WebMethod]
        public static List<CENConcepto> ListarCoceptos(int flag)
        {
            List<CENConcepto> listaconcepto = null;
            CLNConcepto objCLNConcepto = null;
            try
            {
                objCLNConcepto = new CLNConcepto();
                listaconcepto = objCLNConcepto.ListarCoceptos(flag);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = flag.ToString();
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "ListarCoceptos", datosLog, ex);
            }

            return listaconcepto;
        }

        [WebMethod]
        public static List<CENBanco> ListarBanco(int flag)
        {
            List<CENBanco> listabanco = null;
            CLNConcepto objCLNConcepto = null;
            try
            {
                objCLNConcepto = new CLNConcepto();
                listabanco = objCLNConcepto.ListarBanco(flag);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = flag.ToString();
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "ListarBanco", datosLog, ex);
            }

            return listabanco;
        }

        [WebMethod]
        public static CENMensajeDescuento reg_edit_descuento(CENRegistrarDescuento datosDescuento)
        {
            CENMensajeDescuento mensajeRegistrar = new CENMensajeDescuento();
            CLNDescuento objCLNDescuento = null;
            try
            {
                objCLNDescuento = new CLNDescuento();
                mensajeRegistrar = objCLNDescuento.reg_edit_descuento(datosDescuento);
                
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = datosDescuento.ToString();
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "cambiarEstado", datosLog, ex);

                ex.StackTrace.ToString();
                mensajeRegistrar.flag = CENConstante.g_const_500;
                mensajeRegistrar.ntraDescuento = CENConstante.g_const_0;
                mensajeRegistrar.mensaje = CENConstante.g_msj_error_con;
            }
            return mensajeRegistrar;
        }

        [WebMethod]
        public static List<CENListarDescuento> ListarDescuento(int codMarca, int CodTipo, string codBanco, int codCategoria, int codEstado, string codFechaI, string codFechaF)
        {
            List<CENListarDescuento> listaDeatlle = null;
            CLNDescuento objCLNDescuento = null;
            try
            {
                objCLNDescuento = new CLNDescuento();
                listaDeatlle = objCLNDescuento.ListarDescuento(codMarca, CodTipo, codBanco, codCategoria, codEstado, codFechaI, codFechaF);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = codMarca + " | " + CodTipo + " | " + codBanco + " | " + codCategoria + " | " + codEstado + " | " + codFechaI + " | " + codFechaF;
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "ListarDescuento", datosLog, ex);
            }

            return listaDeatlle;
        }

        [WebMethod]
        public static CENMensajeDescuento cambiarEstado(int idDesc, int estado)
        {
            CENMensajeDescuento mensajEstado = null;
            CLNDescuento objCLNDescuento = null;
            try
            {
                objCLNDescuento = new CLNDescuento();
                mensajEstado = objCLNDescuento.cambiarEstado(idDesc, estado);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = idDesc + " | " + estado;
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "cambiarEstado", datosLog, ex);
            }

            return mensajEstado;
        }

        [WebMethod]
        public static int eliminarDescuento(int ntraDescuentos)
        {
            int ok = 0;
            try
            {
                ok = new CADDescuento().eliminarDescuento(ntraDescuentos);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = ntraDescuentos.ToString();
                objCLNError.ejecutarLog("frmMantDescuento.aspx.cs", "cambiarEstado", datosLog, ex);
            }
            return ok;
        }

        [WebMethod]
        public static int existeBin(int codm, int ttar, string codb)
        {
            int response = 1;
            try
            {
                response = new CADBin().existeBin(codm, ttar, codb);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return response;
        }

    }

}