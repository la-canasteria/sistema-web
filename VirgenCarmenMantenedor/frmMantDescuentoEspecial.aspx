﻿<%@ Page ClientIDMode="Static" Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="frmMantDescuentoEspecial.aspx.cs" Inherits="VirgenCarmenMantenedor.frmMantDescuentoEspecial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <!-- LIBRERIAS EXPORTAR EN EXCEL -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

    <script src="Scripts/DescuentoEspecial.js"></script>
    <style>
        body{
            background-image:none;
            background-color: white;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="title_frm col-xl-12">
            MANTENEDOR DE DESCUENTO ESPECIAL
        </div>
    </div>
    <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Filtros de Busqueda
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_categoria">Categoria</label>
                                </div>
                                <select class="custom-select" id="cbo_categoria" runat="server">
                                    <option selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="col-xl-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_banco">Banco</label>
                                </div>
                                <select class="custom-select" id="cbo_banco" runat="server">
                                    <option selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="col-xl-4">
                            <button type="button" class="btn btn-primary" id="btn_buscar" >BUSCAR</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <br />
    <br />

    <div class="table-responsive">
        <table id="tbl_descuento_especial" class="table table-primary table-bordered table-hover nowrap" >
            <thead>
                <tr class="bg-primary">
                    <th scope="col">N° DESCUENTO</th>
                    <th scope="col">COD_CATEGORIA</th>
                    <th scope="col">CATEGORIA</th>
                    <th scope="col">COD_BANCO</th>
                    <th scope="col">BANCO</th>
                    <th scope="col">COD_TIPO_TARJETA</th>
                    <th scope="col">TIPO TARJETA</th>
                    <th scope="col">FECHA I</th>
                    <th scope="col">FECHA F</th>
                    <th scope="col">HORA I</th>
                    <th scope="col">HORA F</th>
                    <th scope="col">COD_ESTADO</th>
                    <th scope="col">ESTADO</th>
                    <th scope="col">ACCION</th>
                    <th scope="col">tiposTarjetas</th>
                </tr>
            </thead>
                
            <tbody id="tbl_body_table">
                
            </tbody>
        </table>
        <br />

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" id="btn_agregar" data-toggle="modal" data-target="#modalRegDescuento" >AGREGAR</button>
    
    </div>
    <br />
  
    <!-- Modal Registrar -->
    <div class="modal fade" id="modalRegDescuento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="title_modal">REGISTRAR DESCUENTO ESPECIAL</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="frm_registrar">
                    <div class="modal-body">

                        <%-- Codigo Descuento --%>
                        <input type="hidden" id="id_cod_descuento"/>

                        <%-- Mensajes de Errore --%>
                        <div id="divErrores">
                            <div id="listaErrores"></div>
                        </div>

                        <%-- Rango de Fechas --%>
                        <div class="form-group">
                            <div class="form-row ml-1">
                                <label class="label">Fecha Vig.</label>
                            </div>
                            <div class="form-row">
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" id="fecha_i_R" name="fecha_i_R" placeholder="F. Inicio" />
                                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" id="fecha_f_R" name="fecha_f_R" placeholder="F. Fin" />
                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%-- Rango de Horas --%>
                        <div class="form-group">
                            <div class="form-row ml-1">
                                <label class="label">Hora</label>
                            </div>
                            <div class="form-row">
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker3" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3" id="hora_i_R" name="hora_i_R" placeholder="H. Inicio" />
                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                </div>
                                <div class="input-group date col-md-6 mb-2" id="datetimepicker4" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4" id="hora_f_R" name="hora_f_R" placeholder="H. Fin" />
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Activar -->
                        <div class="form-group">
                            <label class="label label-default mr-2 ">Activar</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="activacion" id="id_activado" value="1">
                              <label class="form-check-label" for="inlineRadio1">SI</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="activacion" id="id_desactivado" value="2">
                              <label class="form-check-label" for="inlineRadio2">NO</label>
                            </div>
                        </div>

                        <%-- Categoria --%>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_categoriaR">Categoria</label>
                                </div>
                                <select class="custom-select" id="cbo_categoriaR" name="cbo_categoriaR" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>

                        <%-- Banco --%>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="cbo_bancoR">Banco</label>
                                </div>
                                <select class="custom-select" id="cbo_bancoR" name="cbo_bancoR" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                </select>
                            </div>
                        </div>

                        <%-- Tipo de Tarjeta --%>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="tipo_tarjetaR">Tipo Tarjeta</label>
                                </div>
                                <select class="custom-select" id="tipo_tarjetaR" name="tipo_tarjetaR" runat="server">
                                    <option value="0" selected>SELECCIONAR</option>
                                    
                                    <option value="999">TODAS</option>
                                    <option value="888">ALGUNAS</option>
                                    
                                </select>
                            </div>
                        </div>

                        <%-- Elegir Tarjeta --%>
                        <div class="form-group" id="div_tipo_tarjeta">
                            <div class="form-row ml-1">
                                <label class="label">Elegir Tarjetas:</label>
                            </div>
                            <div class="form-group" id="div_tipos_tarjetas_R">
                                <%--
                                <div class="form-row ml-1">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="">
                                        <label class="form-check-label">CREDITO BBVA BLACK</label>
                                    </div>
                                </div>
                                <div class="form-row ml-1">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="">
                                        <label class="form-check-label">CREDITO BBVA BLACK</label>
                                    </div>
                                </div>
                                --%>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCancelarR">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btn_registrar">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Modificar -->
    <div class="modal fade" id="modalEditDescuento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Datos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Cod. BIN</label>
                                <input type="number"  class="form-control" id="cod_BIN_mod" min="0" max="999999"/>
                                <input type="hidden"  class="form-control" id="cod_registro_mod" />
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="inputName">Marca</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_marca_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="marca_mod" />
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <div class="form-group col-xs-6">
                                <label for="inputName">Tipo Trajeta</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_tipoTrajeta_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="tipoTrajeta_mod" />
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="inputName">Banco</label>
                                <%--<input type="text" readonly="readonly" class="form-control" id="id_banco_mod" />--%>
                                <input type="text" readonly="readonly" class="form-control" id="banco_mod" />
                            </div>
                        </div>

                        <div class="form-group" style="text-align: right">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnActualizar">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
