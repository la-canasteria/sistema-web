﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="VirgenCarmenMantenedor.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <title>Login-Sistema de ventas</title>
    
    <!-- Libreria Jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <!-- Libreria Iconos -->
    <%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>--%>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>--%>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"/>
    
    <!-- Libreria Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>

    <!-- Libreria Validaciones -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>

    <!-- AlertifyJS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    
    <script src="Scripts/frmLogin.js"></script>
    <link href="css/login.css" rel="stylesheet" />

</head>
<body class="d-flex flex-column justify-content-center ">
    <div id="loader" class="loader">
        <div class="d-flex justify-content-center align-items-center h-100">
            <div class="spinner-border" role="status">
                <span class="sr-only"></span>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row bg-white p-4 rounded">
            <div class="col-lg-4 col-sm-12 d-flex align-items-center mb-5  ">
                <img class="img-fluid" src="Imagenes/logoCanasteria.png" alt="logo_canasteria"/>
            </div>
            <div class="col-lg-4 d-lg-block d-none text-center">
                <p>Registra los mejores descuentos para tus clientes y sube tus ventas</p>
                <img class="img-fluid " src="Imagenes/PersonaTarjeta.gif" alt="Tarjeta"/>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="box-header  text-center">
                    <h4 class="box-title">Sistema de Descuentos</h4>
                </div>
                <form class="formulario" id="form">
                    <div class="form-group">
                        <label class="col-sm-2 label label-default"  for="exampleInputEmail1">Usuario</label>
                        <div class="input-group">
                            <div class="input-group-append">
							    <span class="input-group-text"><i class="fa fa-user"></i></span>
						    </div>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 label label-default"  for="exampleInputPassword1">Password</label>
                        <div class="input-group">
                            <div class="input-group-append">
							    <span class="input-group-text"><i class="fa fa-lock"></i></span>
						    </div>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="exampleInputPassword1"/>
                            <div class="input-group-prepend">
                                <button id="btnVerPass" class="input-group-text"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group class botones">
                        <button type="submit" class="btn btn-success btn-block" id="ingresar">Ingresar</button>
                    </div>
                    <div class="mensajes" id="mensajes">
                    </div>
                </form>
            </div>
        </div>
        <div id="divErrores" class="mt-4">
            <div id="lista"></div>
        </div>
    </div>
</body>
</html>
