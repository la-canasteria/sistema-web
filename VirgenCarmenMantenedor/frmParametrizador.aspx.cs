﻿using CAD;
using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmParametrizador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarBancos();
            cargarMarca();
        }

        public void cargarBancos()
        {
            try
            {
                List<CENBancos> lista = new CADBancos().ListarBancos(4);
                banco.Items.Clear();
                banco.Items.Add(new ListItem("SELECCIONAR", "0"));
                banco_reg.Items.Clear();
                banco_reg.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemLista in lista)
                {
                    string item = itemLista.codigo.ToString();
                    var descripcion = itemLista.descripcion;
                    banco.Items.Add(new ListItem(descripcion, item));
                    banco_reg.Items.Add(new ListItem(descripcion, item));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        public void cargarMarca()
        {
            try
            {
                List<CENConcepto> lista = new CADMarca().listarMarca(1);
                marca.Items.Clear();
                marca.Items.Add(new ListItem("SELECCIONAR", "0"));
                marca_reg.Items.Clear();
                marca_reg.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemLista in lista)
                {
                    string item = itemLista.codigo.ToString();
                    var descripcion = itemLista.descripcion;
                    marca.Items.Add(new ListItem(descripcion, item));
                    marca_reg.Items.Add(new ListItem(descripcion, item));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        [WebMethod]
        public static List<CENBancos> ListarBancos(int flag)
        {
            List<CENBancos> listaBancos = null;
            CLNBancos objCLNBancos = null;
            try
            {
                objCLNBancos = new CLNBancos();
                listaBancos = objCLNBancos.ListarBancos(flag);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = flag.ToString();
                objCLNError.ejecutarLog("frmParamatrizador.aspx.cs", "ListarBancos", datosLog, ex);
            }
            return listaBancos;
        }
        
        [WebMethod]
        public static List<CENBinLista> ListarBIN(int codMarca, string codBanco, int codTipoTarjeta)
        {
            List<CENBinLista> lista_bin = null;
            CENBin objCENBinDatos = null;
            CLNBin objCLNBin = null;

            try
            {
                objCLNBin = new CLNBin();
                objCENBinDatos = new CENBin(codMarca, codBanco, codTipoTarjeta);
                lista_bin = objCLNBin.ListarBIN(objCENBinDatos);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = codMarca + " | " + codBanco + " | " + codTipoTarjeta;
                objCLNError.ejecutarLog("frmParamatrizador.aspx.cs", "ListarBIN", datosLog, ex);
            }
            return lista_bin;
        }
        
        [WebMethod]
        public static int ElimiarBIN(int id)
        {
            CLNBin objCLNBin = null;
            int ok = -1;
            CENBinLista OBJbIN = new CENBinLista()
            {
                id = id,
            };
            try
            {
                objCLNBin = new CLNBin();
                ok = objCLNBin.ElimiarBIN(OBJbIN);
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = id.ToString();
                objCLNError.ejecutarLog("frmParamatrizador.aspx.cs", "ElimiarBIN", datosLog, ex);
            }
            return ok;
        }
        
        [WebMethod]
        public static CENResponseStoreProcedure InsertarBIN(string codBinReg, int marcaTarjReg, int tipoTarjReg, string bancoReg, string nomUsuario)
        {
            CENResponseStoreProcedure response = new CENResponseStoreProcedure();
            CLNBin objCLNBin = null;
            CENBinInsert objBin = new CENBinInsert()
            {
                codBINInsert = Convert.ToString(codBinReg),
                codMarcaInsert = Convert.ToInt32(marcaTarjReg),
                codTipoInsert = Convert.ToInt32(tipoTarjReg),
                codBancoInsert = Convert.ToString(bancoReg),
                usuarioInsert = Convert.ToString(nomUsuario),


            };
            try
            {
                objCLNBin = new CLNBin();
                response = objCLNBin.InsertarBIN(objBin);
                
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = codBinReg + " | " + marcaTarjReg + " | " + tipoTarjReg + " | " + bancoReg + " | " + nomUsuario;
                objCLNError.ejecutarLog("frmParamatrizador.aspx.cs", "InsertarBIN", datosLog, ex);

                response.codRespuesta = 0;
                response.descRespuesta = "NO SE PUDO REALIZAR LA OPERACION";
            }

            return response;
        }
        
        [WebMethod]
        public static int ActualizarBIN(string BIN, int id)
        {
            int ok = -1;
            CENBinActualizar objBinAct = new CENBinActualizar()
            {
                codBINActualizar = Convert.ToString(BIN),
                idActualizar = Convert.ToInt16(id)

            };
            CLNBin objCLNBin = null;

            try
            {
                objCLNBin = new CLNBin();
                ok = objCLNBin.ActualizarBIN(objBinAct, id);
                
            }
            catch (Exception ex)
            {
                CLNError objCLNError = new CLNError();
                string datosLog = BIN + " | " + id;
                objCLNError.ejecutarLog("frmParamatrizador.aspx.cs", "ActualizarBIN", datosLog, ex);
            }
            return ok;
        }


    }
}