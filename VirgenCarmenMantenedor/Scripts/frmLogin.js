﻿$(document).ready(function () {

    var usuario = $("#exampleInputEmail1");
    var password = $("#exampleInputPassword1");
    var ingresar = $("#ingresar");
    var loader = $('#loader');
    var form_login = $("#form");
    
    $("#btnVerPass").click(function (e) {
        e.preventDefault();
        var pwd = $("#exampleInputPassword1");
        if (pwd.attr('type') == 'password') {
            pwd.attr('type', 'text');
            $("#btnVerPass i").removeClass().addClass("fa fa-eye-slash");
        } else {
            pwd.attr('type', 'password');
            $("#btnVerPass i").removeClass().addClass("fa fa-eye");
        }
    });

    ingresar.click(function () {
        form_login.validate({
            rules: {
                "exampleInputEmail1": { required: true },
                "exampleInputPassword1": { required: true }
            },
            messages: {
                "exampleInputEmail1": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Debe ingresar Usuario<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "exampleInputPassword1": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Debe ingresar Contraseña<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" }
            },
            errorContainer: $("#divErrores"),
            errorLabelContainer: "#divErrores #lista",
            errorElement: "span",
            submitHandler: function (form) {
                var flag = true;
                var json = JSON.stringify({
                    user: usuario.val(),
                    pass: password.val(),
                    intentos: 0
                });
                $.ajax({
                    type: "POST",
                    url: "login.aspx/consultarDatos",
                    data: json,
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        alertify.error("No se pudo realizar la operacion");
                    },
                    beforeSend: function () {
                        loader.show();
                    },
                    success: function (data) {
                        var result = data.d;
                        if (result.codigo == 0) {
                            $(location).attr('href', 'panelControl.aspx');
                        } else {
                            console.log(result.descripcion);
                            alertify.error("No se pudo acceder");
                        }
                    },
                    complete: function () {
                        loader.hide();
                    }
                });
            }
        });
    });

});
