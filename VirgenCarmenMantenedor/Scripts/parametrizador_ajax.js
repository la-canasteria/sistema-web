﻿$(document).ready(function () {
    $("Body").css("background", "none");
    $("#idFrmPrametrizador").css("background-color", "white");

    var loader = $("#loader");

    //Variables para botones

    var btnEditar =   ' <button type = "button" title = "Editar" class="btn btn-primary btn-sm mr-1 btnEditar" data-toggle="modal" data-target="#modalModBIN"><i class="fa fa-pencil" ></i></button>';
    var btnEliminar = ' <button type = "button" title="Eliminar" class="btn btn-danger  btn-sm mr-1 btnDelete" data-toggle="tooltip"><i class="fa fa-trash-o"></i></button>';

    //Variables para tablas
    var tblBIN = $("#tb_BIN");

    //VARIABLES BUSCCAR

    var marca = $("#marca");
    var tipoTarjeta = $("#tipoTarjeta");
    var banco = $("#banco");

    var btnBuscar = $("#id_btnBuscar");

    //VARIABLES MODAL REGISTRAR

    var btnAgregar = $("#buttonModal");
    var cod_BIN_reg = $("#cod_BIN_reg");
    var tipoTarjeta_reg = $("#tipoTarjeta_reg");
    var banco_reg = $("#banco_reg");
    var marca_reg = $("#marca_reg");
    var btnRegistrar = $("#btnRegistrar");
    var btnCancelar = $("#btnCancelarR");


    //VARIABLES MODAL EDITAR

    var cod_registro_mod = $("#cod_registro_mod");
    var cod_BIN_mod = $("#cod_BIN_mod");
    var marca_mod = $("#marca_mod");
    var tipoTrajeta_mod = $("#tipoTrajeta_mod");
    var banco_mod = $("#banco_mod");

    var btnActualizar = $("#btnActualizar");

    //CREAR TABLA
    tblBIN.DataTable({
        paging: false,
        info: false,
        language: {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        columnDefs: [
            { "targets": [1, 3, 5], "width": "15%" },
            { "targets": [9], "width": "10%" },
            { "targets": [0, 2, 4, 6, 8], "visible": false, "searchable": false },
            { "targets": [1, 3, 9], "className": "text-center" }
        ],
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excel',
            sheetName: 'Reporte',
            title: 'REPORTE DE PARAMETRIZACION BIN',
            text: 'Exportar',
            filename: "Reporte BIN",
            exportOptions: {
                columns: [1, 3, 5, 7]
            },
            message: function () {

                var filtrosReporte = "Marca: ";
                if (marca.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#marca option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Banco: ";
                if (banco.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#banco option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Tipo de Tarjeta: ";
                if (tipoTarjeta.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#tipoTarjeta option:selected').text();
                }
                return filtrosReporte;
            }
        }]
    });

    var table = tblBIN.DataTable(); //funcion DataTable-libreria
    table.columns.adjust().draw();

    function llenarTabla(data) {
        //DESCRIPCION : Funcion para llenar la tabla de preventas
        for (var i = 0; i < data.length; i++) {

            var botones = btnEditar + btnEliminar;
            if (data[i].marcaBaja === 0) {
                botones = btnEditar + btnEliminar;
            } else {
                botones = null;
            }

            table.row.add([
                data[i].id,
                data[i].BIN,
                data[i].idMarca,
                data[i].descMarca,
                data[i].idTipo,
                data[i].descTipo,
                data[i].codBanco,
                data[i].descBanco,
                data[i].marcaBaja,
                botones
            ]).draw();
        }

        $('body').on('click', '.btnEditar', function () {
            var tr = $(this).parent().parent();
            llenarModal(table.row(tr));
        });

        $("body").on('click', '.btnDelete', function () {
            var tr = $(this).parent().parent();
            var id = table.row(tr).data()[0];
            swal({
                title: "Se eliminara el registro",
                text: "¿Esta seguro que desea eliminar el registro",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
                //Promesa que me trae el valor true al confirmar OK.
                .then((willDelete) => {
                    if (willDelete) {
                        ElimiarBIN(id);
                    } else {
                        swal("Se Cancelo la eliminación");
                    }
                });
        });
    }
    
    //LISTAR TIPO TARJETA VISTA PRINCIPAL
    function ListarTipo(selected) {
        //DESCRIPCION : Funcion que me trae la lista de clientes -----------------------------------------------------------------
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarTipo",
            data: "{'banco': '" + selected + "' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo listar tipos de tarjeta"
                });
            },
            success: function (data) {
                addTipo(data.d);
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    banco.on('change', function () {
        var selected = $(this).val();
        ListarTipo(selected);
    });

    function addTipo(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de clientes.
        tipoTarjeta.empty();
        tipoTarjeta.append("<option value = 0>SELECCIONAR</option>");
        for (var i = 0; i < data.length; i++) {
            tipoTarjeta.append("<option value=" + data[i]["codigo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    //LISTAR TIPO TARJETA VISTA PRINCIPAL
    function ListarTipoReg(selected) {
        //DESCRIPCION : Funcion que me trae la lista de clientes -----------------------------------------------------------------
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarTipo",
            data: "{'banco': '" + selected + "' }",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo listar tipos de tarjeta"
                });
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addTipoReg(data.d);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    banco_reg.on('change', function () {
        var selected = $(this).val();
        ListarTipoReg(selected);
    });

    function addTipoReg(data) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de clientes.
        tipoTarjeta_reg.empty();
        tipoTarjeta_reg.append("<option value = 0> -Seleccionar- </option>");
        for (var i = 0; i < data.length; i++) {
            tipoTarjeta_reg.append("<option value=" + data[i]["codigo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }
    
    //PROCESOS -----------------------------------------------------------------------------------------------------------------------

    btnAgregar.click(function() {
        cod_BIN_reg.val("");
        tipoTarjeta_reg.val(0);
        banco_reg.val(0);
        marca_reg.val(0);
    });

    function InsertarBIN(data) {
        //Descricion : Recibo los parametros del evento click del GuardadoDeDatosModal
        $.ajax({
            type: "POST",
            url: "frmParametrizador.aspx/InsertarBIN",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo insertar BIN"
                });
            },
            success: function (data) {
                let response = data.d;
                if (response.codRespuesta == 1) {

                    swal(response.descRespuesta, {
                        icon: "success"
                    }).then(respuesta => { 
                            if (respuesta) {
                                limpiarReg();
                                btnCancelar.click();
                                btnBuscar.click();
                            }
                        }
                    );
                    
                } else {

                    swal(response.descRespuesta, {
                        icon: "info"
                    });
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function GurdarDatosModal() {
        //DESCRIPCION : Esta funcion me dispara un evento click,
        // para hacer el guardado de datos, Llamando a la funcion InsertarRutasAsignadas
        btnRegistrar.click(function () {

            var BIN = cod_BIN_reg.val();

            var optionMarcaTarjeta = marca_reg.find("option:selected");
            var marca = optionMarcaTarjeta.val();

            var optionTipoTarjeta = tipoTarjeta_reg.find("option:selected");
            var tipoTarjeta = optionTipoTarjeta.val();

            var optionBanco = banco_reg.find("option:selected");
            var banco = optionBanco.val();


            if (BIN.length === 0 || BIN.length < 6 || BIN === '') {
                swal("El código BIN debe tener 6 caracteres", {
                    icon: "info"
                });
                cod_BIN_reg.focus();
            } else {
                if (marca === 0) {
                    swal("Debe seleccionar una marca de Tarjeta", {
                        icon: "info"
                    });
                    marca_reg.focus();
                } else {
                    if (tipoTarjeta === 0) {
                        swal("Debe seleccionar un tipo de Tarjeta", {
                            icon: "info"
                        });
                        tipoTarjeta_reg.focus();
                    } else {
                        if (banco === '') {
                            swal("Debe seleccionar un banco", {
                                icon: "info"
                            });
                            banco_reg.focus();
                        } else {
                            var json = JSON.stringify({
                                codBinReg: BIN,
                                marcaTarjReg: marca,
                                tipoTarjReg: tipoTarjeta,
                                bancoReg: banco,
                                nomUsuario: nomUsuario
                            });

                            console.log(json);

                            InsertarBIN(json);
                            console.log(json);
                        }
                    }
                }
            }
        });
    }

    GurdarDatosModal();

    function EnviarDatos() {
        //DESCRIPCION : Funcion para enviar los datos de los filtros.
        $(btnBuscar).on('click', function () {

            var codMarca = marca.find("option:selected").val();
            var codBanco = banco.find("option:selected").val();
            var codTipoTarjeta = tipoTarjeta.find("option:selected").val();
            var usuario = nomUsuario;

            var json = JSON.stringify({
                codMarca: codMarca,
                codBanco: codBanco,
                codTipoTarjeta: codTipoTarjeta
            });
            ListarBIN(json);
            obtFechHora();
        });
    }

    EnviarDatos();

    function ListarBIN(datos) {
        //alert(datos);
        //DESCRIPCION : Funcion que me trae la lista de preventas
        $.ajax({
            type: "POST",
            url: "frmParametrizador.aspx/ListarBIN",
            data: datos,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo listar BIN"
                });
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                table.clear().draw();
                llenarTabla(data.d);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    let dfecha1;
    let dfecha2;
    let dhora;

    function obtFechHora() {
        dfecha1 = new Date();
        dfecha2 = dfecha1.getDate() + "/" + (dfecha1.getMonth() + 1) + "/" + dfecha1.getFullYear();
        if (dfecha1.getMinutes() < 10) {
            dhora = dfecha1.getHours() + ":0" + dfecha1.getMinutes();
        } else {
            dhora = dfecha1.getHours() + ":" + dfecha1.getMinutes();
        }
    }

    let nomUsuario;  //variable para guardar el nombre de usuario

    function obtenerUser() {
        $.ajax({
            type: "POST",
            url: "login.aspx/ObtenerUser",
            data: "",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo obtener usuario"
                });
            },
            success: function (data) {
                nomUsuario = data.d;
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    obtenerUser();

    //Eliminar Registro
    function ElimiarBIN(id) {

        var json = JSON.stringify({ id: id });
        $.ajax({
            type: "POST",
            url: "frmParametrizador.aspx/ElimiarBIN",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo eliminar BIN"
                });
            },
            success: function () {
                swal("Se elimino Registro", {
                    icon: "success"
                }).then(() => {
                    $(btnBuscar).click();
                    });
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function llenarModal(table) {
        console.log(table.data());

        cod_registro_mod.val(table.data()[0]);
        cod_BIN_mod.val(table.data()[1]);
        marca_mod.val(table.data()[3]);
        tipoTrajeta_mod.val(table.data()[5]);
        banco_mod.val(table.data()[7]);

    }

    function ActualizarBIN(data) {
        //Descricion : Recibo los parametros del evento click del GuardadoDeDatosModal
        $.ajax({
            type: "POST",
            url: "frmParametrizador.aspx/ActualizarBIN",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo actualizar BIN"
                });
            },
            success: function (response) {
                var result = response.d;
                if (result >= 0) {
                    swal("Se actualizó correctamente", {
                        icon: "success"
                    }).then(() => {
                        $(btnBuscar).click();
                    });
                } else {
                    swal({
                        icon: "error",
                        text: "No se pudo actualizar BIN"
                    });
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function GuardarDatosModalEditar() {

        btnActualizar.click(function () {

            var BIN = cod_BIN_mod.val();
            var id = cod_registro_mod.val();

            if (BIN.length === 0 || BIN.length < 6) {
                swal("El cÓdigo BIN debe tener 6 caracteres", {
                    icon: "info"
                });
                cod_BIN_mod.focus();
            }
            else {
                var obj = JSON.stringify({
                    id: id,
                    BIN: BIN
                });
                
                ActualizarBIN(obj);
                $("#modalModBIN").modal('hide');//ocultamos el modal

            }
        });
    }

    GuardarDatosModalEditar();

    //LIMPIAR CAMPOS

    function limpiarReg() {

        cod_BIN_reg.val("");
        marca_reg.val(0);
        tipoTarjeta_reg.val(0);
        banco_reg.val(0);
    }

    //VALIDAR PARA QUE INGRESE SOLO 6 NUMEROS

    //modal editar
    var input = document.getElementById('cod_BIN_mod');
    input.addEventListener('input', function () {
        if (this.value.length > 6)
            this.value = this.value.slice(0, 6);
    });

    //modal registrar
    var input_reg = document.getElementById('cod_BIN_reg');
    input_reg.addEventListener('input', function () {
        if (this.value.length > 6)
            this.value = this.value.slice(0, 6);
    });

    btnBuscar.click();
});

