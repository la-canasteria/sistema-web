﻿$(document).ready(function () { // Ejecuta codigo cuando la pagina este "ready"
    var btn_buscar = $("#btn_buscar");
    var id_fechaI = $("#id_fechaI");
    var id_fechaF = $("#id_fechaF");
    var cbo_bancos = $("#cbo_bancos");
    var cbo_categorias = $("#cbo_categorias");
    var datetimepicker1 = $("#datetimepicker1");
    var datetimepicker2 = $("#datetimepicker2");

    // Formato de Fechas //
    datetimepicker1.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    datetimepicker2.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    //variables tabla
    $("#tbl_ventas").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [0] },
            { "width": "35%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "15%", "targets": [3] },
            { "width": "20%", "targets": [4] },
            { "width": "20%", "targets": [5] },
            { "width": "5%", "targets": [6] },
            { "width": "15%", "targets": [7] },
            { "width": "20%", "targets": [8] },
            { "width": "10%", "targets": [9] }
        ],

        dom: 'Bfrtip',
        buttons: [{
            extend: 'excel',
            sheetName: 'Reporte',
            title: 'REPORTE DE VENTAS POR DESCUENTOS',
            text: 'Exportar',
            className: "btn_exportar",
            filename: "Reporte Ventas Descuentos",
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            message: function () {

                var filtrosReporte = "";
                if (id_fechaI.val() !== "") {
                    filtrosReporte = "Fecha Desde: " + $('#id_fechaI').val() + " Hasta:" + $('#id_fechaF').val() + " |";
                }

                if (cbo_categorias.val() == 0) {
                    filtrosReporte = filtrosReporte + " Categoria: TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + "Categoria: " + $('#cbo_categorias option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Banco: ";
                if (cbo_bancos.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#cbo_bancos option:selected').text();
                }
                return filtrosReporte;
            }
        }]
    });
    var tbl_ventas = $("#tbl_ventas").DataTable();

    function llenarTabla(data) {
        for (var i = 0; i < data.length; i++) {
            tbl_ventas.row.add([
                data[i].codvent,
                data[i].fechped,
                data[i].desarti,
                data[i].categoria,
                data[i].descbanco,
                data[i].tarjeta,
                data[i].cantive,
                data[i].importe,
                data[i].impdscto,
                data[i].impvent
            ]).draw();
        }
    }

    btn_buscar.click(function buscarDescuento() {
        var fechainicial = id_fechaI.val();
        var fechafinal = id_fechaF.val();
        var codbanco = cbo_bancos.val();
        var codcategoria = cbo_categorias.val();

        if ((fechainicial !== "" && fechafinal == "") || (fechainicial == "" && fechafinal !== "")) {
            alert("RANGO DE FECHAS INCORRECTO..");
        } else {
            var array_fecha_i = fechainicial.split("/");
            var array_fecha_f = fechafinal.split("/");
            var fechaf = new Date(array_fecha_f[1] + "-" + array_fecha_f[0] + "-" + array_fecha_f[2]);
            var fechai = new Date(array_fecha_i[1] + "-" + array_fecha_i[0] + "-" + array_fecha_i[2]);
            if (fechaf < fechai) {
                alert("FECHA INICIAL MAYOR A LA FECHA FINAL..");
            } else {
                $.ajax({
                    type: "POST",
                    url: "ReporteVentasDescuentos.aspx/listaventasDescuentos",
                    data: "{'fechin':'" + fechainicial + "','fechfin':'" + fechafinal + "', 'banco':'" + codbanco + "', 'categoria':'" + codcategoria + "'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr + ajaxOtions + thrownError);
                        alert("NO SE PUDO OBTENER VENTAS");
                    },
                    success: function (listaventas) {
                        tbl_ventas.clear().draw(); //limpiando tabla antes de llenar
                        llenarTabla(listaventas.d);
                    }
                });

            }
        }
    });

    btn_buscar.click();
});