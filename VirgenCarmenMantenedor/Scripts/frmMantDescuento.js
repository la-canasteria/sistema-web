﻿$(document).ready(function () {

    var loader = $('#loader');

    //Lista de Variables para filtros
    let fechaInicial = $("#id_fechaI");
    let fechaFinal = $("#id_fechaF");
    let id_marca = $("#id_marca");
    let id_tipo = $("#id_tipo");
    let id_banco = $("#id_banco");
    let id_categoria = $("#id_categoria");
    let id_estado = $("#id_estado");
    //Variables para modal registrar / editar
    let id_fecha_vigI = $("#id_fecha_vigI");
    let id_fecha_vigF = $("#id_fecha_vigF");
    let id_horaI = $("#id_horaI");
    let id_horaF = $("#id_horaF");
    let id_activado = $("#id_activado");
    let id_desactivado = $("#id_desactivado");
    let M_marca = $("#M_marca");
    let M_tipo = $("#M_tipo");
    let M_banco = $("#M_banco");
    //Fleming v2
    let M_categoria = $("#M_categoria");
    //Fleming v2
    let id_cantidad = $("#id_cantidad");
    //Variables adicionales
    let id_tituloDesc = $("#id_tituloDesc");
    let proceso = 0;
    let ntraDescuento = 0;
    let flagestado = 0;
    let msjFecha = $("#msjFecha");
    let duser;
    let dfecha1;
    //Variables para Botones
    let btnAgregar = $("#btnAgregar");
    let btnBuscar = $("#btnBuscar");
    let btnRegistrar = $("#btnregistrar");
    let btnCancelar = $("#btncancelar");
    //Variables para botones de la tabla
    let btnEditar = ' <button type = "button" title = "Editar"   class="btn btn-primary btn-sm mr-1 btnModificar" data-toggle="modal" data-target="#modalDescuento"><i class="fa fa-pencil" ></i></button>';
    let btnAnular = ' <button type = "button" title = "Eliminar" class="btn btn-danger  btn-sm mr-1 btnAnular   "                                                  ><i class="fa fa-trash-o"></i></button>';
    //Variables para tablas
    let tbldescuento = $("#id_tblDescuento");

    $('#modalDescuento').on('shown.bs.modal', function () {
        $('#id_fecha_vigI').trigger('focus');
    });

    btnAgregar.click(function () {
        limpiarReg_Edit();
        id_tituloDesc.html("REGISTRAR DESCUENTO");
        btnRegistrar.html("REGISTRAR");
        ntraDescuento = 0;
        proceso = 1;
    });

    function validarRegistroDescuento() {
        btnRegistrar.click(function () {
            if (id_fecha_vigI.val() === "") {
                swal("Debe seleccionar la fecha inicial", {
                    icon: "warning"
                });
            } else {
                if (id_fecha_vigF.val() === "") {
                    //event.preventDefault();
                    swal("Debe seleccionar la fecha final", {
                        icon: "warning"
                    });
                } else {
                    if (id_horaI.val() === "") {
                        swal("Debe seleccionar la hora inicial", {
                            icon: "warning"
                        });
                    } else {
                        if (id_horaF.val() === "") {
                            swal("Debe seleccionar la hora final", {
                                icon: "warning"
                            });
                        } else {
                            if (M_marca.val() === "0") {
                                //event.preventDefault();
                                swal("Debe seleccionar una marca de tarjeta", {
                                    icon: "warning"
                                });
                            } else {
                                if (M_banco.val() === "0") {
                                    //event.preventDefault();
                                    swal("Debe seleccionar un banco", {
                                        icon: "warning"
                                    });
                                } else {
                                    if (M_tipo.val() === "0") {
                                        //event.preventDefault();
                                        swal("Debe seleccionar un tipo de tarjeta", {
                                            icon: "warning"
                                        });
                                    } else {
                                        //Fleming v2
                                        if (M_categoria.val() === "0") {
                                            //event.preventDefault();
                                            swal("Debe seleccionar una categoria", {
                                                icon: "warning"
                                            });
                                        } else {
                                        //Fleming v2
                                            if (id_cantidad.val() === "") {
                                                //event.preventDefault();
                                                swal("Debe ingresar el porcentaje a descontar", {
                                                    icon: "warning"
                                                });
                                            } else {
                                                var array_fecha_i = id_fecha_vigI.val().split("/");
                                                var array_fecha_f = id_fecha_vigF.val().split("/");
                                                var fechaf = new Date(array_fecha_f[1] + "-" + array_fecha_f[0] + "-" + array_fecha_f[2]);
                                                var fechai = new Date(array_fecha_i[1] + "-" + array_fecha_i[0] + "-" + array_fecha_i[2]);
                                                if (fechaf < fechai) {
                                                    swal({
                                                        icon: "warning",
                                                        title: "FECHA INICIAL MAYOR A LA FECHA FINAL.."
                                                    });
                                                } else {
                                                    existeBIN();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        });
    }

    validarRegistroDescuento();

    function existeBIN() {
        var json = JSON.stringify({
            codm: M_marca.val(),
            ttar: M_tipo.val(),
            codb: M_banco.val()
        });

        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/existeBin",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                if (data.d == 0) {
                    swal({
                        title: "No existe BIN asociado",
                        text: "¿Esta seguro que desea continuar?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    }).then((willDelete) => {
                        if (willDelete) {
                            reg_edit_descuento();
                        } else {
                            swal("Registro Cancelado");
                        }
                    });
                } else {
                    reg_edit_descuento();
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function reg_edit_descuento() {

        var jsonReg = JSON.stringify({
            datosDescuento: {
                proceso: proceso,
                descripcion: M_marca.find("option:selected").text() + " | " + M_tipo.find("option:selected").text() + " | " + M_banco.find("option:selected").text() + " | " + M_categoria.find("option:selected").text(),
                fechaVigenciaI: id_fecha_vigI.val(),
                fechaVigenciaF: id_fecha_vigF.val(),
                horaI: id_horaI.val(),
                horaF: id_horaF.val(),
                flagestado: flagestado,
                codMarca: M_marca.val(),
                CodTipo: M_tipo.val(),
                CodBanco: M_banco.val(),
                CodCategoria: M_categoria.val(),
                codCantidad: id_cantidad.val(),
                ntraDescuento: ntraDescuento,
                nomUsuario: duser
            }
        });

        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/reg_edit_descuento",
            data: jsonReg,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                let datos = data.d;
                if (datos.flag == 0) {
                    mostrarMensajeConfirmacion(datos, "success");

                } else {
                    mostrarMensajeConfirmacion(datos, "error");
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }
        
    function mostrarMensajeConfirmacion(respuesta, icon) {
        swal(respuesta.mensaje, "EL DESCUENTO CON CODIGO: " + respuesta.ntraDescuento, icon)
        .then((willDelete) => {
            if (willDelete) {
                btnCancelar.click();
                btnBuscar.click();
            }
        });
    }

    //Convertimos la tabla preventa en dataTable y le pasamos parametros
    tbldescuento.DataTable({
        paging: false,
        info: false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        order: [[0, "asc"]],
        columnDefs: [
            { "targets": [3], "width": "20%" },
            { "targets": [12, 13, 14, 15, 16], "visible": false, "searchable": false },
            { "targets": [0, 7, 8, 9, 10, 13], "className": "text-center" },
            { "targets": [5], "className": "text-right" }
        ],
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excel',
            sheetName: 'Reporte',
            title: 'REPORTE DE DESCUENTOS',
            text: 'Exportar',
            filename: "Reporte Descuentos",
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
            message: function () {

                var filtrosReporte = "Categoria: ";
                if (id_categoria.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#id_categoria option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Banco: ";
                if (id_banco.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#id_banco option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Tipo Tarjeta: ";
                if (id_tipo.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#id_tipo option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Marca Tarjeta: ";
                if (id_marca.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#id_marca option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Estado: ";
                if (id_estado.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#id_estado option:selected').text();
                }

                if (fechaInicial.val() !== '') {
                    filtrosReporte = filtrosReporte + " | Fecha Inicial: " + fechaInicial.val();
                }

                if (fechaFinal.val() !== '') {
                    filtrosReporte = filtrosReporte + " | Fecha Final: " + fechaFinal.val();
                }

                return filtrosReporte;
            }
        }]
    });
    
    let table = tbldescuento.DataTable(); //funcion DataTable-libreria
    table.columns.adjust().draw();

    function enviarDatos() {
        btnBuscar.click(function () {
            var fechainicial = fechaInicial.val();
            var fechafinal = fechaFinal.val();

            if ((fechainicial !== "" && fechafinal == "") || (fechainicial == "" && fechafinal !== "")) {
                swal({
                    icon: "warning",
                    title: "RANGO DE FECHAS INCORRECTO.."
                });
            } else {
                var array_fecha_i = fechainicial.split("/");
                var array_fecha_f = fechafinal.split("/");
                var fechaf = new Date(array_fecha_f[1] + "-" + array_fecha_f[0] + "-" + array_fecha_f[2]);
                var fechai = new Date(array_fecha_i[1] + "-" + array_fecha_i[0] + "-" + array_fecha_i[2]);
                if (fechaf < fechai) {
                    swal({
                        icon: "warning",
                        title: "FECHA INICIAL MAYOR A LA FECHA FINAL.."
                    });
                } else {
                    let codMarca = id_marca.val();
                    let CodTipo = id_tipo.val();
                    let codBanco = id_banco.val();
                    let codCategoria = id_categoria.val();
                    let codEstado = id_estado.val();
                    let codFechaI = fechaInicial.val();
                    let codFechaF = fechaFinal.val();
                    let jsonData = JSON.stringify({
                        codMarca: codMarca,
                        CodTipo: CodTipo,
                        codBanco: codBanco,
                        codCategoria: codCategoria,
                        codEstado: codEstado,
                        codFechaI: codFechaI,
                        codFechaF: codFechaF
                    });
                    listarDescuentos(jsonData);
                    obtFechHora();
                }
            }
        });
    }

    enviarDatos();

    function listarDescuentos(datos) {
        //DESCRIPCION: Obtener los descuentos de la base de datos
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarDescuento",
            data: datos,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                table.clear().draw();
                llenarTabla(data.d);
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function llenarTabla(data) {
        //DECRIPCION: Llenar la tabla de descuentos
        var botones = btnEditar + btnAnular;
        for (var i = 0; i < data.length; i++) {
            table.row.add([
                data[i].ntraDescuento,    //0
                data[i].desMarca,
                data[i].desTipo,
                data[i].desBanco,
                data[i].descCategoria,
                data[i].descuento.toFixed(2),
                data[i].desEstado,  //5
                data[i].fechaInicial, 
                data[i].fechaFin,
                data[i].horaInicial.substring(0, 5),
                data[i].horaFin.substring(0, 5),
                botones,    //10
                data[i].codEstado,
                data[i].codMarca,
                data[i].codTipo,
                data[i].codBanco,
                data[i].codCategoria
            ]).draw();
        }
    }
    
    $('body').on('click', '.btnModificar', function () {
        limpiarReg_Edit();
        id_tituloDesc.html("EDITAR DESCUENTO");
        btnRegistrar.html("ACTUALIZAR");
        proceso = 2;

        let tr2 = $(this).parent().parent();

        ntraDescuento = table.row(tr2).data()[0];

        let estad = table.row(tr2).data()[12];
        if (estad == 1) {
            id_activado.click();
        } else {
            id_desactivado.click();
        }

        let fechI = table.row(tr2).data()[7];
        let fechF = table.row(tr2).data()[8];
        id_fecha_vigI.val(fechI);
        id_fecha_vigF.val(fechF);
        id_horaI.val(table.row(tr2).data()[9]);
        id_horaF.val(table.row(tr2).data()[10]);
        M_marca.val(table.row(tr2).data()[13]).prop('disabled', true);
        M_banco.val(table.row(tr2).data()[15]).prop('disabled', true);

        M_tipo.empty();
        M_tipo.append("<option value = " + table.row(tr2).data()[14] + ">" + table.row(tr2).data()[2] + "</option>");
        M_tipo.prop('disabled', true);

        M_categoria.val(table.row(tr2).data()[16]);
        id_cantidad.val(table.row(tr2).data()[5]);
    });

    $('body').on('click', '.btnAnular', function () {
        var rowDelete = $(this).parent().parent();
        var ntraDescuentos = table.row(rowDelete).data()[0];

        swal({
            title: "Se eliminara el registro",
            text: "¿Esta seguro que desea eliminar el registro",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if (willDelete) {
                eliminarDescuento(ntraDescuentos);
            } else {
                swal("Se Cancelo la eliminación");
            }
        });
    });

    function obtFechHora() {
        dfecha1 = new Date();
        dfecha2 = dfecha1.getDate() + "/" + (dfecha1.getMonth() + 1) + "/" + dfecha1.getFullYear();
        if (dfecha1.getMinutes() < 10) {
            dhora = dfecha1.getHours() + ":0" + dfecha1.getMinutes();
        } else {
            dhora = dfecha1.getHours() + ":" + dfecha1.getMinutes();
        }
    }

    function obtenerUser() {
        $.ajax({
            type: "POST",
            url: "login.aspx/ObtenerUser",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo obtener usuario");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                duser = data.d;
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    obtenerUser();

    function eliminarDescuento(ntraDescuentos) {
        var json = JSON.stringify({
            ntraDescuentos: ntraDescuentos
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/eliminarDescuento",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo eliminar"
                });
            },
            success: function (data) {
                var datos = data.d;
                if (datos > 0) {
                    swal("Se elimino Registro", {
                        icon: "success"
                    }).then(() => {
                        $(btnBuscar).click();
                    });
                } else {
                    swal("No se pudo eliminar Registro", {
                        icon: "error"
                    }).then(() => {
                        $(btnBuscar).click();
                    });
                }
                
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function cambiarEstado(idDesc, nuevoEstado) {
        //DESCRIPCION : Funcion pra cambiar el estado del descuento
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/cambiarEstado",
            data: "{'idDesc': '" + idDesc + "' , 'estado': '" + nuevoEstado + "'}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                mensajeEstado(data.d);
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    function mensajeEstado(resp) {
        swal(resp.mensaje, "EL DESCUENTO CON CODIGO: " + resp.ntraDescuento, "success")
        .then((willDelete) => {
            if (willDelete) {
                btnBuscar.click();
            }
        });
    }

    //LISTAR CAMPOS -----------------------------------------------------------------------------------------------------------------------
    
    function ListarMArca() {
        var json = JSON.stringify({
            flag: 1
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarCoceptos",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar marca");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo(data.d, id_marca);
                addCombo(data.d, M_marca);
            },
            complete: function () {
                loader.hide();
            }
        });

    }

    ListarMArca();

    function ListarTipo(selected, combo) {
        var json = JSON.stringify({
            banco: selected
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarTipo",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar tipo tarjeta");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo(data.d, combo);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    //ListarTipo();
    id_banco.on('change', function () {
        var selected = $(this).val();
        ListarTipo(selected, id_tipo);
    });

    M_banco.on('change', function () {
        var selected = $(this).val();
        ListarTipo(selected, M_tipo);
    });

    function ListarEstado() {
        var json = JSON.stringify({
            flag: 3
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarCoceptos",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar conceptos tipo 3");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo(data.d, id_estado);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    ListarEstado();

    function ListarBanco() {
        var json = JSON.stringify({
            flag: 4
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarBanco",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar bancos");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo2(data.d, id_banco);
                addCombo2(data.d, M_banco);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    ListarBanco();
    
    function ListarCategoria() {
        var json = JSON.stringify({
            flag: 5
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarCoceptos",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar conceptos tipo 5");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo(data.d, id_categoria);
                addCombo(data.d, M_categoria);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    ListarCategoria();
    //Fleming v2

    function addCombo(data, combo) {
        combo.empty();
        combo.append("<option value = 0>SELECCIONAR</option>");
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de clientes.
        for (var i = 0; i < data.length; i++) {
            combo.append("<option value=" + data[i]["codigo"] + ">" + data[i]["descripcion"] + "</option>");
        }
    }

    function addCombo2(data, combo) {
        //DESCRIPCION : Funcion para llenar la lista de opciones del select de clientes.
        for (var i = 0; i < data.length; i++) {
            combo.append("<option value=" + data[i]["codigo"] + ">" + data[i]["abreviatura"] + "  -  " + data[i]["descripcion"] + "</option>");
        }
    }

    //VALIDACIONES -----------------------------------------------------------------------------------------------------------------------
    function limpiarReg_Edit() {
        id_fecha_vigI.val("");
        id_fecha_vigF.val("");
        id_horaI.val("00:00");
        id_horaF.val("23:59");
        id_activado.click();
        id_cantidad.val("");
        M_marca.val('0').prop('disabled', false);
        M_tipo.val('0').prop('disabled', false);
        M_banco.val('0').prop('disabled', false);
        M_categoria.val('0').prop('disabled', false);
    }

    function validarActivacion() {
        id_activado.click(function () {
            flagestado = 1;
        });
        id_desactivado.click(function () {
            flagestado = 2;
        });
    }

    validarActivacion();

    $("#datetimepicker1").datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    $("#datetimepicker2").datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    $("#datetimepicker3").datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    $("#datetimepicker4").datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    $("#datetimepicker5").datetimepicker({
        locale: 'es',
        format: "LT"
    });

    $("#datetimepicker6").datetimepicker({
        locale: 'es',
        format: "LT"
    });

    //$(".fvalidar").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    weekHeader: 'Sm',
    //    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    //    dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
    //    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
    //    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
    //    dateFormat: 'dd/mm/yy',
    //    language: "es",
    //    showAnim: "show"
    //});

    //function validarfecha() {
    //    $('.fvalidar').change(function () {
    //        var minR = fechaInicial.val();
    //        var maxR = fechaFinal.val();
    //        var expreFormato = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
    //        if ((minR !== "" && maxR !== "")) {
    //            if (!(expreFormato.test(minR.trim())) || !(expreFormato.test(maxR.trim()))) {
    //                mensValFechaHora(msjFecha, "Debe ingresar el formato valido (dd/mm/yyyy)");
    //                fechaInicial.val("");
    //                fechaFinal.val("");
    //            }
    //            else if ($.datepicker.parseDate('dd/mm/yy', maxR) < $.datepicker.parseDate('dd/mm/yy', minR)) {
    //                mensValFechaHora(msjFecha, "Fecha inicial debe ser menor a la fecha final");
    //                fechaFinal.val("");
    //            }
    //        }
    //    });
    //}

    //validarfecha();

    //$('.fvalidarEdit').datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    weekHeader: 'Sm',
    //    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    //    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    //    dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
    //    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
    //    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
    //    dateFormat: 'dd/mm/yy',
    //    //showWeek: true,
    //    firstDay: 1,
    //    language: "es",
    //    showAnim: "show",
    //    minDate: 0
    //});

    //function valfechaEdit() {
    //    $('.fvalidarEdit').change(function () {
    //        var minR = id_fecha_vigI.val();
    //        var maxR = id_fecha_vigF.val();
    //        var expreFormato = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
    //        if ((minR !== "" && maxR !== "")) {
    //            if (!(expreFormato.test(minR.trim())) || !(expreFormato.test(maxR.trim()))) {
    //                mensValFechaHora(msjFechaEdit, "Debe ingresar el formato valido (dd/mm/yyyy)");
    //                id_fecha_vigI.val("");
    //                id_fecha_vigF.val("");
    //            }
    //            else if ($.datepicker.parseDate('dd/mm/yy', maxR) < $.datepicker.parseDate('dd/mm/yy', minR)) {
    //                mensValFechaHora(msjFechaEdit, "Fecha inicial debe ser menor a la fecha final");
    //                id_fecha_vigF.val("");
    //            }
    //        }
    //    });
    //}

    //valfechaEdit();

    //function valhoraEdit() {
    //    $('.hvalidarEdit').blur(function () {
    //        var minR = id_horaI.val();
    //        var maxR = id_horaF.val();
    //        if ((minR !== "" && maxR !== "") && (maxR < minR)) {
    //            mensValFechaHora(msjHoraEdit, "Hora inicial debe ser menor a la hora final");
    //            //id_horaI.val("");
    //            id_horaF.val(minR);
    //            //id_horaI.focus();
    //        }
    //    });
    //}

    //valhoraEdit();

    //function mensValFechaHora(idmens, mensaje) {
    //    idmens.removeAttr("hidden");
    //    idmens.html("<h5>" + mensaje + "</h5>");
    //    idmens.css('height', '30px');
    //    idmens.css("color", "#ffffff");
    //    idmens.css("background-color", "#EC340F")
    //        .css("border-color", "#2e6da4");
    //    setTimeout(function () {
    //        idmens.attr("hidden", true);
    //    }, 3000);
    //}

    function validarCamposNum() {
        id_cantidad.change(function () {
            if (id_cantidad.val() <= 0) {
                id_cantidad.val("");
            }
            if (id_cantidad.val() >= 100) {
                id_cantidad.val(99.99);
            }
        });
    }

    validarCamposNum();

    btnBuscar.click();

});

