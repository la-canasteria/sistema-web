﻿$(document).ready(function () { //aqui se ejecuta todo lo que esta dentro, cuando la pagina esta lista
    var id_fechaI = $("#id_fechaI");
    var id_fechaF = $("#id_fechaF");
    var cbo_bancos = $("#cbo_bancos");
    var btn_buscar = $("#btn_buscar");
    var datetimepicker1 = $("#datetimepicker1");
    var datetimepicker2 = $("#datetimepicker2");

    // Formato de Fechas //
    datetimepicker1.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    datetimepicker2.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });

    //variables tabla
    $("#tbl_ventasb").DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "width": "5%", "targets": [0] },
            { "width": "35%", "targets": [1] },
            { "width": "15%", "targets": [2] },
            { "width": "15%", "targets": [3] },
            { "width": "20%", "targets": [4] }
        ],

        dom: 'Bfrtip',
        buttons: [{
            extend: 'excel',
            sheetName: 'Reporte',
            title: 'REPORTE DE VENTAS POR BANCOS',
            text: 'Exportar',
            className: "btn_exportar",
            filename: "Reporte Ventas Bancos",
            exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            message: function () {
                var filtrosReporte = "";

                if (id_fechaI.val() !== "") {
                    filtrosReporte = "Fecha Desde: " + $('#id_fechaI').val() + " Hasta:" + $('#id_fechaF').val() + " |";
                }


                filtrosReporte = filtrosReporte + "  Banco: ";
                if (cbo_bancos.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#cbo_bancos option:selected').text();
                }
                return filtrosReporte;
            }
        }]
    });

    var tbl_ventasb = $("#tbl_ventasb").DataTable();
    function llenarTabla(data) {
        for (var i = 0; i < data.length; i++) {
            tbl_ventasb.row.add([
                data[i].descbanco,
                data[i].tarjeta,
                data[i].impdscto,
                data[i].cantive,
                data[i].imptven
            ]).draw();
        }
    }

    btn_buscar.click(function buscarDescuento() {
        var codbanco = cbo_bancos.val();
        var fechainicial = id_fechaI.val();
        var fechafinal = id_fechaF.val();

        if ((fechainicial !== "" && fechafinal == "") || (fechainicial == "" && fechafinal !== "")) {
            alert("RANGO DE FECHAS INCORRECTO..");
        } else {
            var array_fecha_i = fechainicial.split("/");
            var array_fecha_f = fechafinal.split("/");
            var fechaf = new Date(array_fecha_f[1] + "-" + array_fecha_f[0] + "-" + array_fecha_f[2]);
            var fechai = new Date(array_fecha_i[1] + "-" + array_fecha_i[0] + "-" + array_fecha_i[2]);
            if (fechaf < fechai) {
                alert("FECHA INICIAL MAYOR A LA FECHA FINAL..");
            } else {
                $.ajax({
                    type: "POST",
                    url: "ReporteVentasBancos.aspx/listaventasBancos",
                    data: "{'fechin':'" + fechainicial + "','fechfin':'" + fechafinal + "', 'banco':'" + codbanco + "'}",
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr, ajaxOtions, thrownError) {
                        console.log(xhr + ajaxOtions + thrownError);
                        alert("NO SE PUDO OBTENER VENTAS");
                    },
                    success: function (listaventas) {
                        tbl_ventasb.clear().draw(); //limpiando tabla antes de llenar
                        llenarTabla(listaventas.d);
                    }
                });

            }
        }
    });

    btn_buscar.click();
});