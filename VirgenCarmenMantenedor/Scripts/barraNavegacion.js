﻿function cerrarSesion() {
    fetch('login.aspx/CerrarSesion', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; Scharset=utf-8'
        }
    })
        .then(response => response.json())
        .then(data => {
            if (parseInt(data.d[0].respuesta, 10) == 2000) {
                let p = document.getElementById('tituloSistema');
                p.innerHTML = " ";
                location.href = 'login.aspx';
            }
            else {
                alertify.error("se produjo un error al intentar cerrar la sesion");
            }
        })
        .then(e => console.log(e));
}

function credenciales() {

    fetch('login.aspx/getSession', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; Scharset=utf-8'
        }
    })
        .then(response => response.json())
        .then(data => {

            if (data.d[0].nombre == "") {
                location.href = 'login.aspx';
            } else {
                let p = document.getElementById('tituloSistema');
                //p.innerHTML = data.d[0].nombre + " - " + data.d[0].perfil;
                p.insertAdjacentHTML("afterend",'<a href="#" class="navbar-link" id="cerrarSesion">Cerrar sesión  <span class="glyphicon glyphicon-log-out"> </span></a>');
                document.getElementById('cerrarSesion').addEventListener('click', cerrarSesion);
            }
        })
        .catch(e => console.log(e));
}

addEventListener('load', credenciales);
