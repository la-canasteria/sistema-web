﻿$(document).ready(function () {

    var loader = $('#loader');

    /** FILTROS DE BUSQUEDAD **/
    var cbo_categoria = $("#cbo_categoria");
    var cbo_banco = $("#cbo_banco");
    var btn_buscar = $("#btn_buscar");
    
    /** TABLA DESCUENTOS **/
    var tbl_dscto_especial = $("#tbl_descuento_especial");

    /** REGISTRAR DESCUENTO **/
    var title_modal = $("#title_modal");
    var id_cod_descuento = $("#id_cod_descuento");
    var btn_agregar = $("#btn_agregar");
    var frm_registrar = $("#frm_registrar");
    var fecha_i_R = $("#fecha_i_R");
    var fecha_f_R = $("#fecha_f_R");
    var hora_i_R = $("#hora_i_R");
    var hora_f_R = $("#hora_f_R");
    var id_activado = $("#id_activado");
    var id_desactivado = $("#id_desactivado");
    var cbo_categoriaR = $("#cbo_categoriaR");
    var cbo_bancoR = $("#cbo_bancoR");
    var tipo_tarjetaR = $("#tipo_tarjetaR");
    var div_tipo_tarjeta = $("#div_tipo_tarjeta");
    var div_tipos_tarjetas_R = $("#div_tipos_tarjetas_R");
    var datetimepicker1 = $('#datetimepicker1');
    var datetimepicker2 = $('#datetimepicker2');
    var datetimepicker3 = $('#datetimepicker3');
    var datetimepicker4 = $('#datetimepicker4');
    var btn_registrar = $("#btn_registrar");
    var btnCancelarR = $("#btnCancelarR");

    /* Cambiar los atributos "name" de los elementos que cambiaron por el runat('server') */
    fecha_i_R.attr("name", function () { return $(this).attr("id"); });
    fecha_f_R.attr("name", function () { return $(this).attr("id"); });
    hora_i_R.attr("name", function () { return $(this).attr("id"); });
    hora_f_R.attr("name", function () { return $(this).attr("id"); });
    cbo_categoriaR.attr("name", function () { return $(this).attr("id"); });
    cbo_bancoR.attr("name", function () { return $(this).attr("id"); });
    tipo_tarjetaR.attr("name", function () { return $(this).attr("id"); });

    /******* AGREGAR VALIDACION VALIDATE *******/
    jQuery.validator.addMethod("dif", function (value, element, param) {
        return this.optional(element) || value != param;
    }, "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Debe seleccionar Banco<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>");
   
    tbl_dscto_especial.DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        },
        "columnDefs": [
            { "targets": [3], "width": "20%" },
            { "targets": [1, 3, 5, 11, 14], "visible": false, "searchable": false },
            { "targets": [0, 7, 8, 9, 10, 13], "className": "text-center" }
        ],
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excel',
            sheetName: 'Reporte',
            title: 'REPORTE DE DESCUENTO ESPECIAL',
            text: 'Exportar',
            filename: "Reporte Descuento Especial",
            exportOptions: {
                columns: [0,2,4,6,7,8,9,10,12]
            },
            message: function () {

                var filtrosReporte = "Categoria: ";
                if (cbo_categoria.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#cbo_categoria option:selected').text();
                }

                filtrosReporte = filtrosReporte + " | Banco: ";
                if (cbo_banco.val() == 0) {
                    filtrosReporte = filtrosReporte + "TODOS";
                }
                else {
                    filtrosReporte = filtrosReporte + $('#cbo_banco option:selected').text();
                }
                return filtrosReporte;
            }
        }]
    });

    var tb_dscto_especial = tbl_dscto_especial.DataTable();
    tb_dscto_especial.columns.adjust().draw();

    datetimepicker1.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });
    
    datetimepicker2.datetimepicker({
        locale: 'es',
        format: "DD/MM/YYYY"
    });
    
    datetimepicker3.datetimepicker({
        locale: 'es',
        format: "LT"
    });

    datetimepicker4.datetimepicker({
        locale: 'es',
        format: "LT"
    });
    
    /****** BUSCAR *******/

    function llenarTabla(data) {
        var botones =
            '<button type = "button" title = "Ver Detalle" class="btn btn-success btn-sm mr-1 btnVer"      data-toggle="modal" data-target="#modalRegDescuento" ><i class="fa fa-eye"    ></i></button>' +
            '<button type = "button" title = "Editar"      class="btn btn-primary btn-sm mr-1 bntEditar"   data-toggle="modal" data-target="#modalRegDescuento" ><i class="fa fa-pencil" ></i></button>' +
            '<button type = "button" title = "Eliminar"    class="btn btn-danger  btn-sm mr-1 btnEliminar"                                                      ><i class="fa fa-trash-o"></i></button>' ;
        for (var i = 0; i < data.length; i++) {
            tb_dscto_especial.row.add([
                data[i].codDescuento,
                data[i].codCategoria,
                data[i].descCategoria,
                data[i].codBanco,
                data[i].descBanco,
                data[i].codTarjeta,
                data[i].descTarjeta,
                data[i].fechai,
                data[i].fechaf,
                data[i].horai,
                data[i].horaf,
                data[i].codEstado,
                data[i].descEstado,
                botones,
                data[i].tiposTarjetas
            ]).draw();
        }
    }

    btn_buscar.click(function () {
        var json = JSON.stringify({
            categoria: cbo_categoria.val(),
            banco: cbo_banco.val()
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuentoEspecial.aspx/buscarDescuento",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo buscar descuentos");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                tb_dscto_especial.clear().draw();
                llenarTabla(data.d);
            },
            complete: function () {
                loader.hide();
            }
        });


    });

    btn_buscar.click();

    /******** REGISTRAR DESCUENTO *******/
    
    function grabarDescuentoEspecial(cod_tarjeta) {
        var json = JSON.stringify({
            dataReg : {
                codDescuento: id_cod_descuento.val(),
                descripcion: cbo_bancoR.find("option:selected").text() + " | " + cbo_categoriaR.find("option:selected").text(),
                fechaInicial: fecha_i_R.val(),
                fechaFin: fecha_f_R.val(),
                horaInicial: hora_i_R.val(),
                horaFin: hora_f_R.val(),
                flagestado: $('input:radio[name=activacion]:checked').val(),
                tipoTarjeta: tipo_tarjetaR.val(),
                codBanco: cbo_bancoR.val(),
                codCategoria: cbo_categoriaR.val(),
                cod_tarjeta: cod_tarjeta
            }
        });

        $.ajax({
            type: "POST",
            url: "frmMantDescuentoEspecial.aspx/grabarDescuentoEspecial",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                swal({
                    icon: "error",
                    text: "No se pudo realizar la operacion"
                });
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                var respuesta = data.d;
                if (respuesta.codError > 0) {
                    swal({
                        icon: "success",
                        title: respuesta.descError,
                        text: "Codigo de Descuento es: " + respuesta.codError
                    }).then(() => {
                        btnCancelarR.click();
                        btn_buscar.click();
                    });
                } else {
                    swal({
                        icon: "warning",
                        title: respuesta.descError
                    });
                }
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    btn_agregar.click(function () {
        resetearCampos();
        /* Limpiar campos */
        title_modal.html("REGISTRAR DESCUENTO");
        id_cod_descuento.val("");
        fecha_f_R.val("");
        fecha_i_R.val("");
        hora_f_R.val("23:59");
        hora_i_R.val("00:00");
        id_activado.click();
        cbo_categoriaR.val(0);
        cbo_bancoR.val(0);
        tipo_tarjetaR.val(0);
        div_tipo_tarjeta.hide();
        tipo_tarjetaR.empty();
        tipo_tarjetaR.append("<option value = '0'>SELECCIONAR</option>");
        $("#frm_registrar .alert-danger").hide();
    });

    function existeBIN(cod_tarjeta) {
        var jsonBIN = JSON.stringify({
            'codm': '0',
            'ttar': '0',
            'codb': cbo_bancoR.val()
        });

        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/existeBin",
            data: jsonBIN,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                if (data.d == 0) {
                    swal({
                        title: "No existe BIN asociado",
                        text: "¿Esta seguro que desea continuar?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    }).then((willDelete) => {
                        if (willDelete) {
                            grabarDescuentoEspecial(cod_tarjeta);
                        } else {
                            swal("Registro Cancelado");
                        }
                    });
                } else {
                    grabarDescuentoEspecial(cod_tarjeta);
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    btn_registrar.click(function () {
        $("#listaErrores").remove(".alert-danger");
        frm_registrar.validate({
            rules: {
                "fecha_i_R": { required: true },
                "fecha_f_R": { required: true },
                "hora_i_R": { required: true },
                "hora_f_R": { required: true },
                "cbo_categoriaR": { min: 1 },
                "cbo_bancoR": { dif: 0 },
                "tipo_tarjetaR": { min: 1 }
            },
            messages: {
                "fecha_i_R": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(1) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "fecha_f_R": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(2) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "hora_i_R": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(3) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "hora_f_R": { required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(4) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "cbo_categoriaR": { min: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(5) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
                "tipo_tarjetaR": { min: "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" + getMessageError(6) + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>" },
            },
            errorContainer: $("#divErrores"),
            errorLabelContainer: "#divErrores #listaErrores",
            errorElement: "span",
            submitHandler: function (form) {
                var cod_tarjeta = "";
                if (tipo_tarjetaR.val() == 888) {
                    var seleccionardos = $(".check-ttar:checked").length;
                    if (seleccionardos == 0) {
                        $("#divErrores").show();
                        $("#listaErrores").show();
                        $("#listaErrores").append("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Debe seleccionar un tipo de tarjeta<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>x</span></button></div>");
                        return false;
                    } else {
                        cod_tarjeta = "<tarjetas>";
                        $('.check-ttar:checked').each(
                            function () {
                                cod_tarjeta = cod_tarjeta + "<tarjeta>";
                                cod_tarjeta = cod_tarjeta + "<codigo>" + $(this).val() + "</codigo>";
                                cod_tarjeta = cod_tarjeta + "</tarjeta>";
                            }
                        );
                        cod_tarjeta = cod_tarjeta + "</tarjetas>";
                    }
                    existeBIN(cod_tarjeta);
                } else {
                    existeBIN(cod_tarjeta);
                }
            }
        });
    });

    function getMessageError(flag) {
        var msj_error = "";

        switch (flag) {
            case 1:
                msj_error = "Fecha inicial es obligatorio";
                break;
            case 2:
                msj_error = "Fecha final es obligatorio";
                break;
            case 3:
                msj_error = "Hora inicial es obligatorio";
                break;
            case 4:
                msj_error = "Hora final es obligatorio";
                break;
            case 5:
                msj_error = "Debe seleccionar Categoria";
                break;
            case 6:
                msj_error = "Debe seleccionar Tipo de Tarjeta";
                break;
            case 7:
                msj_error = "Debe seleccionar Banco";
                break;
            default:
                msj_error = "Error no definido";
                break;
        }
        return msj_error;
    }

    function mostrarTiposTarjetas(row, ocultar){
        var json = JSON.stringify({
            banco: tb_dscto_especial.row(row).data()[3]
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarTipo",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar tipo tarjeta");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (datos) {
                data = datos.d;
                if (data.length > 0) {
                    $(".div-check-ttar").remove();
                }
                for (var i = 0; i < data.length; i++) {
                    var TipTarj = tb_dscto_especial.row(row).data()[14];
                    var listTipTarj = TipTarj.split("|");
                    var flag_check = 0;
                    for (var j = 0; j < listTipTarj.length; j++) {
                        if (listTipTarj[j] == data[i]["codigo"]) {
                            flag_check = 1;
                            break;
                        }
                    }
                    if (flag_check == 0) {
                        if (ocultar == true) {
                            div_tipos_tarjetas_R.append('<div class="ml-4 div-check-ttar"><input class="form-check-input check-ttar" type="checkbox" value="' + data[i]["codigo"] + '" disabled/><label class="form-check-label" >' + data[i]["descripcion"] + '</label></div>');
                        } else {
                            div_tipos_tarjetas_R.append('<div class="ml-4 div-check-ttar"><input class="form-check-input check-ttar" type="checkbox" value="' + data[i]["codigo"] + '"/><label class="form-check-label" >' + data[i]["descripcion"] + '</label></div>');
                        }
                    } else {
                        if (ocultar == true) {
                            div_tipos_tarjetas_R.append('<div class="ml-4 div-check-ttar"><input class="form-check-input check-ttar" type="checkbox" value="' + data[i]["codigo"] + '" checked disabled/><label class="form-check-label" >' + data[i]["descripcion"] + '</label></div>');
                        } else {
                            div_tipos_tarjetas_R.append('<div class="ml-4 div-check-ttar"><input class="form-check-input check-ttar" type="checkbox" value="' + data[i]["codigo"] + '" checked /><label class="form-check-label" >' + data[i]["descripcion"] + '</label></div>');
                        }
                    }
                    
                }
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    $('body').on('click', '.bntEditar', function () {
        resetearCampos();
        title_modal.html("MODIFICAR DESCUENTO");
        var row = $(this).parent().parent();
        id_cod_descuento.val(tb_dscto_especial.row(row).data()[0]);
        fecha_f_R.val(tb_dscto_especial.row(row).data()[8]);
        fecha_i_R.val(tb_dscto_especial.row(row).data()[7]);
        hora_f_R.val(tb_dscto_especial.row(row).data()[10]);
        hora_i_R.val(tb_dscto_especial.row(row).data()[9]);
        cbo_categoriaR.val(tb_dscto_especial.row(row).data()[1]).prop('disabled', true);
        cbo_bancoR.val(tb_dscto_especial.row(row).data()[3]).prop('disabled', true);

        // Estado
        var estad = tb_dscto_especial.row(row).data()[11];
        if (estad == 1) {
            id_activado.click();
        } else {
            id_desactivado.click();
        }

        // Tipos de Tarjetas
        var tipoTarjeta = tb_dscto_especial.row(row).data()[5];
        if (tipoTarjeta == 999) {
            tipo_tarjetaR.val(tipoTarjeta).prop('disabled', false);
            mostrarTiposTarjetas(row, false);
            div_tipo_tarjeta.hide();
        } else {
            tipo_tarjetaR.val('888').prop('disabled', false);
            mostrarTiposTarjetas(row, false);
            div_tipo_tarjeta.show();
        }
        $("#frm_registrar .alert-danger").hide();
    });

    $('body').on('click', '.btnVer', function () {
        resetearCampos();
        title_modal.html("VER DESCUENTO");
        var row = $(this).parent().parent();
        id_cod_descuento.val(tb_dscto_especial.row(row).data()[0]);
        fecha_f_R.val(tb_dscto_especial.row(row).data()[8]).prop('disabled', true);
        fecha_i_R.val(tb_dscto_especial.row(row).data()[7]).prop('disabled', true);
        hora_f_R.val(tb_dscto_especial.row(row).data()[10]).prop('disabled', true);
        hora_i_R.val(tb_dscto_especial.row(row).data()[9]).prop('disabled', true);
        cbo_categoriaR.val(tb_dscto_especial.row(row).data()[1]).prop('disabled', true);
        cbo_bancoR.val(tb_dscto_especial.row(row).data()[3]).prop('disabled', true);

        // Estado
        var estad = tb_dscto_especial.row(row).data()[11];
        if (estad == 1) {
            id_activado.click();
        } else {
            id_desactivado.click();
        }
        id_activado.prop('disabled', true);
        id_desactivado.prop('disabled', true);

        // Tipos de Tarjetas
        var tipoTarjeta = tb_dscto_especial.row(row).data()[5];
        if (tipoTarjeta == 999) {
            tipo_tarjetaR.val(tipoTarjeta).prop('disabled', true);
            mostrarTiposTarjetas(row, true);
            div_tipo_tarjeta.hide();
        } else {
            tipo_tarjetaR.val('888').prop('disabled', true);
            mostrarTiposTarjetas(row, true);
            div_tipo_tarjeta.show();
        }
        btn_registrar.hide();
        $("#frm_registrar .alert-danger").hide();
    });

    function resetearCampos() {
        title_modal.html("REGISTRAR DESCUENTO");
        id_cod_descuento.val("");
        fecha_f_R.val("").prop('disabled', false);
        fecha_i_R.val("").prop('disabled', false);
        hora_f_R.val("").prop('disabled', false);
        hora_i_R.val("").prop('disabled', false);
        id_activado.prop('disabled', false);
        id_desactivado.prop('disabled', false);
        cbo_categoriaR.val(0).prop('disabled', false);
        cbo_bancoR.val(0).prop('disabled', false);
        tipo_tarjetaR.val(0).prop('disabled', false);
        btn_registrar.show();
        div_tipo_tarjeta.hide();
        $("#frm_registrar .alert-danger").hide();
    }

    function addCombo(data, combo) {
        combo.empty();
        combo.append("<option value = '0'>SELECCIONAR</option>");
        if (data.length > 0) {
            combo.append("<option value = '999'>TODOS</option>");
            combo.append("<option value = '888'>ALGUNOS</option>");
        }
        if (data.length > 0) {
            $(".div-check-ttar").remove();
        }
        for (var i = 0; i < data.length; i++) {
            div_tipos_tarjetas_R.append('<div class="ml-4 div-check-ttar"><input class="form-check-input check-ttar" type="checkbox" value="' + data[i]["codigo"] + '"/><label class="form-check-label" >' + data[i]["descripcion"] + '</label></div>');
        }
        if (data.length > 0) {
            $(".check-ttar").attr('checked', false);
        }
        div_tipo_tarjeta.hide();
    }

    function ListarTipo(selected, combo) {
        var json = JSON.stringify({
            banco: selected
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/ListarTipo",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log("Error: " + xhr + " " + ajaxOtions + " " + thrownError);
                swal("No se pudo listar tipo tarjeta");
            },
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                addCombo(data.d, combo);
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    cbo_bancoR.on('change', function () {
        var selected = $(this).val();
        ListarTipo(selected, tipo_tarjetaR);
    });

    tipo_tarjetaR.on('change', function () {
        var selected = $(this).val();
        if (selected == 888) {
            div_tipo_tarjeta.show();
        } else {
            div_tipo_tarjeta.hide();
        }
    });

    function eliminarDescuento(ntraDescuentos) {
        var json = JSON.stringify({
            ntraDescuentos: ntraDescuentos
        });
        $.ajax({
            type: "POST",
            url: "frmMantDescuento.aspx/eliminarDescuento",
            data: json,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOtions, thrownError) {
                console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
                swal({
                    icon: "error",
                    text: "No se pudo eliminar"
                });
            },
            success: function (data) {
                var datos = data.d;
                if (datos > 0) {
                    swal("Se elimino Registro", {
                        icon: "success"
                    }).then(() => {
                        btn_buscar.click();
                    });
                } else {
                    swal("No se pudo eliminar Registro", {
                        icon: "error"
                    }).then(() => {
                        btn_buscar.click();
                    });
                }
            },
            beforeSend: function () {
                loader.show();
            },
            complete: function () {
                loader.hide();
            }
        });
    }

    $('body').on('click', '.btnEliminar', function () {
        var rowDelete = $(this).parent().parent();
        var ntraDescuentos = tb_dscto_especial.row(rowDelete).data()[0];

        swal({
            title: "Se eliminara el registro",
            text: "¿Esta seguro que desea eliminar el registro",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            //Promesa que me trae el valor true al confirmar OK.
            .then((willDelete) => {
                if (willDelete) {
                    eliminarDescuento(ntraDescuentos);
                } else {
                    swal("Se Cancelo la eliminación");
                }
            });
    });

});

$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});