﻿<%@ Page ClientIDMode="Static" Title="" Language="C#" MasterPageFile="~/MasterPage_Loyout.Master" AutoEventWireup="true" CodeBehind="ReporteVentasBancos.aspx.cs" Inherits="VirgenCarmenMantenedor.ReporteVentasBancos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <!-- LIBRERIAS EXPORTAR EN EXCEL -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="Scripts/ReporteBanco.js"></script>
    <style>
        body{
            background-image:none;
            background-color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="title_frm col-xl-12">
            REPORTE DE VENTAS POR BANCOS
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Filtros de Busqueda
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="cbo_bancos">Banco</label>
                            </div>
                            <select class="custom-select" name="cbo_bancos" id="cbo_bancos" runat="server">
                                <option value="0" selected>SELECCIONAR</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 ml-3">
                        <label class="label">Fecha Pedido</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group date col-md-4 mb-3" id="datetimepicker1" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" id="id_fechaI" name="id_fechaI" placeholder="F. Inicio" />
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="input-group date col-md-4 mb-3" id="datetimepicker2" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" id="id_fechaF" name="id_fechaF" placeholder="F. Fin" />
                        <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                            <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <button type="button" class="btn btn-primary" id="btn_buscar" >BUSCAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />

    <div class="table-responsive">
        <table id="tbl_ventasb" class="table table-primary table-bordered table-hover nowrap">
            <thead>
                <tr class="bg-primary">
                    <th scope="col">BANCO</th>
                    <th scope="col">TIPO DE TARJETA</th>
                    <th scope="col">IMPORTE DESCUENTO</th>
                    <th scope="col">CANTIDAD VENDIDA</th>
                    <th scope="col">IMPORTE TOTAL DE VENTA</th>
                </tr>
            </thead>
            <tbody>
                        
            </tbody>
        </table>
    </div>
    <br />
    <br />
</asp:Content>
