﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class ReporteVentasDescuentos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarBancos();
            cargarCategorias();

        }
        public void cargarBancos()
        {
            try
            {
                List<CENBancos> listBancos = new List<CENBancos>();
                listBancos = new CADBancos().ListarBancos(4);
                cbo_bancos.Items.Clear();
                cbo_bancos.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemEmpresa in listBancos)
                {
                    string codigo = itemEmpresa.codigo.ToString();
                    string nombre = itemEmpresa.descripcion;
                    cbo_bancos.Items.Add(new ListItem(nombre, codigo));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        public void cargarCategorias()
        {
            try
            {
                List<CENConcepto> listCategoria = new List<CENConcepto>();
                listCategoria = new CADConcepto().ListarCoceptos(5);
                cbo_categorias.Items.Clear();
                cbo_categorias.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemEmpresa in listCategoria)
                {
                    string codigo = itemEmpresa.codigo.ToString();
                    string nombre = itemEmpresa.descripcion;
                    cbo_categorias.Items.Add(new ListItem(nombre, codigo));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        [WebMethod]
        public static List<CENReporteVentasDescuentos> listaventasDescuentos(string fechin, string fechfin, string banco, int categoria)
        {
            List<CENReporteVentasDescuentos> listaventas = null;
            CADReporteVentasDescuentos objcadReporte = null;

            try
            {
                objcadReporte = new CADReporteVentasDescuentos();
                listaventas = objcadReporte.ReporteVentasDescuentos(fechin, fechfin, banco, categoria);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return listaventas;

        }
    }



}