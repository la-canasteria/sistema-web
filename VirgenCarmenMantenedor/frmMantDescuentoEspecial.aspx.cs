﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VirgenCarmenMantenedor
{
    public partial class frmMantDescuentoEspecial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarBancos();
            cargarCategorias();
            CargarDescuentosEspeciales();
        }

        public void cargarBancos()
        {
            try
            {
                List<CENBancos> lista = new CADBancos().ListarBancos(4);
                cbo_banco.Items.Clear();
                cbo_banco.Items.Add(new ListItem("SELECCIONAR", "0"));
                cbo_bancoR.Items.Clear();
                cbo_bancoR.Items.Add(new ListItem("SELECCIONAR", "0"));
                foreach (var itemLista in lista)
                {
                    string item = itemLista.codigo.ToString();
                    var descripcion = itemLista.descripcion;
                    cbo_banco.Items.Add(new ListItem(descripcion, item));
                    cbo_bancoR.Items.Add(new ListItem(descripcion, item));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        public void cargarCategorias()
        {
            try
            {
                List<CENConcepto> lista = new CADConcepto().ListarCoceptos(5);

                cbo_categoria.Items.Clear();
                cbo_categoria.Items.Add(new ListItem("SELECCIONAR", "0"));

                cbo_categoriaR.Items.Clear();
                cbo_categoriaR.Items.Add(new ListItem("SELECCIONAR", "0"));

                foreach (var itemLista in lista)
                {
                    string item = itemLista.codigo.ToString();
                    var descripcion = itemLista.descripcion;
                    cbo_categoria.Items.Add(new ListItem(descripcion, item));
                    cbo_categoriaR.Items.Add(new ListItem(descripcion, item));
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        public void CargarDescuentosEspeciales()
        {
            
        }

        [WebMethod]
        public static ResponseDescuentoEspecial grabarDescuentoEspecial(RequestRegDescEspecial dataReg)
        {
            ResponseDescuentoEspecial response = new ResponseDescuentoEspecial();
            try
            {
                string usuario = System.Web.HttpContext.Current.Session["usuario"].ToString();
                response = new CADDescuentoEspecial().grabarDescuentoEspecial(dataReg, usuario);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return response;
        }

        [WebMethod]
        public static List<ResponseBuscarDescuentoEspecial> buscarDescuento(int categoria, string banco)
        {
            List<ResponseBuscarDescuentoEspecial> lista = new List<ResponseBuscarDescuentoEspecial>();

            try
            {
                lista = new CADDescuentoEspecial().buscarDescuento(categoria, banco);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            return lista;
        }
    }
}