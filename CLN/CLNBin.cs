﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNBin
    {

        public List<CENBinLista> ListarBIN(CENBin datos)
        {
            CADBin objCADBin = null;

            try
            {
                objCADBin = new CADBin();
                return objCADBin.ListarBIN(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public CENResponseStoreProcedure InsertarBIN(CENBinInsert objBin)
        {
            CADBin objCLNBin = null;

            try
            {
                objCLNBin = new CADBin();
                return objCLNBin.InsertarBIN(objBin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int ElimiarBIN(CENBinLista objBinAD)
        {

            CADBin objCLNBin = null;
            try
            {
                objCLNBin = new CADBin();
                return objCLNBin.ElimiarBIN(objBinAD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public int ActualizarBIN(CENBinActualizar objBin, int id)
        {
            CADBin objCLNBin = null;
            try
            {
                objCLNBin = new CADBin();
                return objCLNBin.ActualizarBIN(objBin, id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
