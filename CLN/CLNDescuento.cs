﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLNDescuento
    {
        public CENMensajeDescuento reg_edit_descuento(CENRegistrarDescuento datos)
        {
            CADDescuento objCADDescuento = null;

            try
            {
                objCADDescuento = new CADDescuento();
                return objCADDescuento.reg_edit_descuento(datos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENListarDescuento> ListarDescuento(int codMarca, int CodTipo, string codBanco, int codCategoria, int codEstado, string codFechaI, string codFechaF)
        {
            CADDescuento objCADDescuento = null;
            try
            {
                objCADDescuento = new CADDescuento();
                return objCADDescuento.ListarDescuento(codMarca, CodTipo, codBanco, codCategoria, codEstado, codFechaI, codFechaF);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CENMensajeDescuento cambiarEstado(int idDesc, int estado)
        {
            CADDescuento objCADDescuento = null;

            try
            {
                objCADDescuento = new CADDescuento();
                return objCADDescuento.cambiarEstado(idDesc, estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
