﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;
using CAD;

namespace CLN
{
    public class CLNUsuario
    {
        public CENDatosUsuario credencialesUsuario(string usuario, string password, int intentos)
            //DESCRIPCION: Retorna los datos del usuario logueado
        {
            CADUsuario objUsuario;
            try
            {
                objUsuario = new CADUsuario();
                return objUsuario.DatosUsuarios(usuario, password, intentos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
