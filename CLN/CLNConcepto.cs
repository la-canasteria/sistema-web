﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;
using CAD;

namespace CLN
{
    public class CLNConcepto
    {
        public List<CENConcepto> ListarCoceptos(int flag)
        {
            CADConcepto objCADConcepto = null;

            try
            {
                objCADConcepto = new CADConcepto();
                return objCADConcepto.ListarCoceptos(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENConcepto> ListarTipo(String banco)
        {
            CADConcepto objCADConcepto = null;

            try
            {
                objCADConcepto = new CADConcepto();
                return objCADConcepto.ListarTipo(banco);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CENBanco> ListarBanco(int flag)
        {
            CADConcepto objCADConcepto = null;

            try
            {
                objCADConcepto = new CADConcepto();
                return objCADConcepto.ListarBanco(flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}