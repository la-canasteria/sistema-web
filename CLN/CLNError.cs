﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CLN
{
    public class CLNError
    {
        public void registrarLog(CENRequestErrorWeb datos) {

            try {
                new CADError().registrarLog(datos);
            }
            catch (Exception ex) {
                ex.Message.ToString();
            }
        }

        public void ejecutarLog(string formulario, string metodo, string datos, Exception ex) {
            CENRequestErrorWeb objCENRequestErrorWeb = new CENRequestErrorWeb();
            objCENRequestErrorWeb.modulo = formulario + " => " + metodo;
            objCENRequestErrorWeb.opcion = "Datos: " + datos;
            objCENRequestErrorWeb.sistema = "WEB DESCUENTOS";
            objCENRequestErrorWeb.tipo = 1;
            objCENRequestErrorWeb.usuario = HttpContext.Current.Session["codUser"].ToString();
            objCENRequestErrorWeb.descripcion =
                    "Message: " + ex.Message + "\n" +
                    "Source: " + ex.Source;
            new CLNError().registrarLog(objCENRequestErrorWeb);
        }
    }
}
